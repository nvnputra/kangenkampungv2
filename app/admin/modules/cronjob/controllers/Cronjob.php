<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends MY_Controller {

	public function __construct() {
		$this->load->model('M_cronjob');
		$this->load->library('recaptcha');
	}
	public function tracking(){
		$data = $this->M_cronjob->fetch_joins('m__cart', '*', '','kode_pengiriman is not null ', TRUE);
		foreach ($data as $cek) {
			 $this->cek_track($cek['kode_pengiriman'],$cek['id_cart'],$cek['layanan_kurir']);
		}
	}

	public function cek_track($no_resi,$id_cart,$jasa){
		$resi = "waybill=".$no_resi;
        $jasa = '&courier='.$jasa;
		$get  = $this->rajaongkir->cek_tracking($resi.$jasa);
		$hasil = json_decode($get);
		$i = count($hasil->rajaongkir->result->manifest);
		// print_r($hasil->rajaongkir->query);
		 
		if($hasil->rajaongkir->result->delivery_status->pod_receiver == null){
			$pod_receiver 			= "kosong";
		}else{
			$pod_receiver 			= $hasil->rajaongkir->result->delivery_status->pod_receiver;
		}

		if($hasil->rajaongkir->result->delivery_status->pod_time == null){
			$pod_time 			= "kosong";
		}else{
			$pod_time 			= $hasil->rajaongkir->result->delivery_status->pod_time;
		}

		if($hasil->rajaongkir->result->delivery_status->pod_date == null){
			$pod_date 			= "kosong";
		}else{
			$pod_date 			= $hasil->rajaongkir->result->delivery_status->pod_date;
		}

		$city_name				= $hasil->rajaongkir->result->manifest[0]->city_name;

		$manifest_description	= $hasil->rajaongkir->result->manifest[0]->manifest_description;
		$manifest_date			= $hasil->rajaongkir->result->manifest[0]->manifest_date;
		$manifest_time			= $hasil->rajaongkir->result->manifest[0]->manifest_time;

		$manifest_code			= $hasil->rajaongkir->result->manifest[0]->manifest_code;
		$status_cart			= $hasil->rajaongkir->result->delivery_status->status;
		// echo $no_resi;
	    $this->simpan_tracking($pod_receiver,$city_name,$manifest_description,$pod_date,$pod_time,$no_resi,$id_cart,$status_cart,$manifest_code,$manifest_date,$manifest_time);	
	}		


	public function simpan_tracking($pod_receiver,$city_name,$manifest_description,$pod_date,$pod_time,$no_resi,$id_cart,$status_cart,$manifest_code,$manifest_date,$manifest_time){
		    $count_data_detail = $this->M_cronjob->get_count_data('a__tracking');
			$id_tracking = "TRD".date('his').$count_data_detail;
			$cek = $this->M_cronjob->get_row("*","a__tracking","manifest_time ="."'".$manifest_time."'"." AND "."no_resi ="."'".$no_resi."'"." AND id_cart = "."'".$id_cart."'",'','',TRUE);

			if(count($cek) == 0){
						$data = array(
						"id_tracking" => $id_tracking,
						"pod_receiver" => $pod_receiver,
						"city_name" => $city_name,
						"manifest_description" => $manifest_description,
						"pod_date" =>$pod_date,
						"pod_time" =>$pod_time,
						"no_resi" =>$no_resi,
						"id_cart" =>$id_cart,
						"status_tracking" =>$status_cart,
						"manifest_code" =>$manifest_code,
						"manifest_date" => $manifest_date,
						"manifest_time" => $manifest_time
					);
					$add = $this->M_cronjob->insert_table("a__tracking",$data);
					echo "data_ditambahkan";
					if($status_cart == "DELIVERED"){
						$this->update_cart($id_cart);
					}
			}else{
				echo "data sudah ada";
			}
			
	}

	  public function update_cart($id_cart){
		$data = array(
				"status_cart" => 0,
				"update_at" => date("Y-m-d H:i:s")
			);
			$edit = $this->M_cronjob->update_table("m__cart",$data,"id_cart",$id_cart);
	}
}
?>