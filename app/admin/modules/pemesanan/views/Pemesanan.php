<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Pemesanan</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="zoom: 85%">
    <div class="col-sm-6">
        <div class="container-fluid mt--6">
            <div class="card mb-4">
                <!-- Card header -->
                <!-- Card body -->
                <div class="card-body">
                    <label>Member Terdaftar</label>
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="table_terdaftar">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama Pembeli</td>
                                    <!-- <td>No Hp</td> -->
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="container-fluid mt--6">
            <div class="card mb-4">
                <!-- Card header -->
                <!-- Card body -->
                <div class="card-body">
                    <label>Member Tidak Terdaftar</label>
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="table_tdk_terdaftar">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama Pembeli</td>
                                    <!-- <td>No Hp</td> -->
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalDetail" class="modal fade" role="dialog" style="zoom: 90%">
    <div class="modal-dialog modal-xl">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <h1>Detail Pembelian Barang</h1>
                <div class="row">
                    <div class="col-sm-6">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>Nama</td>
                                    <td>Jumlah</td>
                                    <td>Harga</td>
                                </tr>
                            </thead>
                            <tbody id="data_cart">
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <div class="table table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td colspan="2">
                                        <center>Total</center>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h1>
                                            <center>
                                                <div id="total"></div>
                                            </center>
                                        </h1>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td> Nomor Handphone</td>
                                    <td> <input type="text" class="form-control" id="phone" /></td>
                                </tr>
                                <tr>
                                    <td> Email</td>
                                    <td> <input type="text" class="form-control" id="email" readonly /></td>
                                </tr>
                                <tr>
                                    <td> Nama </td>
                                    <td>
                                        <div id="nama"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Alamat</td>
                                    <td>
                                        <textarea id="alamat" class="form-control"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Kode Unik</td>
                                    <td>
                                        <input type="text" class="form-control" id="kode_unik" />
                                    </td>
                                </tr>
                                <tr>
                                    <td> Ongkos Kirim</td>
                                    <td> <input type="number" class="form-control" id="biaya_kirim" /></td>
                                </tr>
                                <tr>
                                    <td> Masukkan RESI</td>
                                    <td><input type="text" class="form-control" id="no_resi" /></td>
                                </tr>
                                <tr>
                                    <td> Status Pesanan</td>
                                    <td><select class="form-control" id="status_pemesanan">
                                            <option value="1">Pesan belum Di Cek</option>
                                            <option value="2">Pesan Telah Dikirim</option>
                                            <option value="3">Bukti Telah Di Upload</option>
                                            <option value="4">Pembelian Berhasil</option>
                                            <option value="5">Barang Dikirim</option>
                                            <option value="6">Barang Sampai</option>
                                            <option value="7">Pesanan Di Batalkan</option>
                                        </select></td>
                                </tr>
                                <div style="display: none">
                                    <input type="text" class="form-control" id="id_order" />
                                    <input type="text" class="form-control" id="id_member" />
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="send_wa()">Kirim Pesan</button>
            </div> -->
        </div>

    </div>
</div>




<?php include("js/js.php"); ?>