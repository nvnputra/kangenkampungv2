<script type="text/javascript">
  $(document).ready(function() {
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    dataTableShow("#table_terdaftar", "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/ajax_action_datatable_terdaftar", form_data);

    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    dataTableShow("#table_tdk_terdaftar", "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/ajax_action_datatable_tidak_terdaftar", form_data);
  });
</script>

<script>
  function detail_pembelian(id, id_member) {
    $('#id_order').val(id);
    $('#id_member').val(id_member);
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/get_data_cart ";
    var html = "";
    var pembayaran = "";
    var total = 0;
    var link = ``;
    var form_data = new FormData();
    form_data.append('id_order', id);
    form_data.append('id_member', id_member);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      $('#phone').val(response.member[0].no_member);
      $('#nama').html(response.member[0].name_member);
      $('#alamat').html(response.alamat[0].provinsi_alamat + "" + response.alamat[0].kota_alamat + "" + response.alamat[0].kecamatan_alamat + "" + response.alamat[0].kelurahan_alamat + "" + response.alamat[0].kode_pos_alamat + "" + response.alamat[0].alamat_lengkap);
      for (i = 0; i < response.data.length; i++) {
        total += (parseInt(response.data[i].subtotal));
        html += `<tr>
                  <td>${response.data[i].name_product}</td>
                  <td>${response.data[i].qty}</td>
                  <td>${addNumber(response.data[i].subtotal,true)}</td>
            </tr>`;
      }
      $('#email').val(response.member[0].email_member);
      $(`#status_pemesanan option[value=${response.order[0].status_order}]`).attr('selected', 'selected');
      if (response.order[0].status_order == 4) {
        $("#no_resi").prop('disabled', false);
      } else {
        $("#no_resi").prop('disabled', true);
      }
      if (response.order[0].status_order > 1) {
        $("#phone").prop('disabled', true);
        $("#biaya_kirim").prop('disabled', true);
        $("#kode_unik").prop('disabled', true);
        $("#kode_unik").val(response.order[0].kode_unik);
        $("#biaya_kirim").val(response.order[0].ongkir);
        $("#no_resi").val(response.order[0].no_resi);
      } else {
        $('#kode_unik').val(Math.round(Math.random() * 100));
        $("#phone").prop('disabled', false);
        $("#biaya_kirim").prop('disabled', false);
        $("#kode_unik").prop('disabled', false);
        $("#no_resi").val(response.order[0].no_resi);
      }
      $('#data_cart').html(html);
      $('#total').html(addNumber(total, true));
      $('#modalDetail').modal("show");
      $("#status_pemesanan").change(function() {
        change_status(1);
      });
    });
  }
</script>
<script>
  function send_wa() {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/get_data_cart";
    var html = "";
    var pembayaran = "";
    var total = 0;
    var link = ``;
    var first_text = "";
    var form_data = new FormData();
    form_data.append('id_order', $('#id_order').val());
    form_data.append('id_member', $('#id_member').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      first_text += `Yth. Bpk/Ibu *${response.member[0].name_member}* ☺

Perkenankan, saya Fara dari *KangenKampung.id*, mau mengonfirmasi bahwa pesanan anda dengan rincian:

*No. Order*: ${$('#id_order').val()}
*Detail Barang*:
`;
      for (i = 0; i < response.data.length; i++) {
        total += (parseInt(response.data[i].subtotal));
        first_text += `-${response.data[i].qty} PCS ${response.data[i].name_product}
`;
      }
      first_text += `
Total belanja : ${addNumber(total,true)},-
Ongkos kirim : ${addNumber($('#biaya_kirim').val(),true)},-
Kode unik : ${addNumber($('#kode_unik').val(),true)},-
Sehingga total keseluruhan menjadi:
*${addNumber((total+(parseInt($('#biaya_kirim').val())+parseInt($('#kode_unik').val()))),true)}*,-

*Dikirim ke alamat:*
${response.alamat[0].provinsi_alamat + " " + response.alamat[0].kota_alamat + " " + response.alamat[0].kecamatan_alamat + " " + response.alamat[0].kelurahan_alamat + " " + response.alamat[0].kode_pos_alamat + " ( " + response.alamat[0].alamat_lengkap+" )"}

Silahkan transfer senilai *${addNumber((total+(parseInt($('#biaya_kirim').val())+parseInt($('#kode_unik').val()))),true)}*, ke salah satu rekening dibawah ini:

BCA
No. Rek: 048 1425 114
Atas Nama: Rakha Hendra Maryanto

Mandiri
No. Rek: 90000 2963 4269
Atas Nama: Rakha Hendra Maryanto


NB:
- Untuk mempercepat proses verifikasi harap transfer sesuai dengan nominal tertera sampai 3 digit angka terakhir
- Setelah melakukan transfer, harap segera konfirmasi dengan mengirim bukti transfer melalui chat di WA ini.

Terima kasih atas kepercayaan anda untuk belanja di toko kami 😊
*KangenKampung.id*`
      // link += ` %0ATotal%20belanja%20%3A%20${addNumber(total,true)}`;
      // link += `%0ADitambah%20ongkos%20kirim%20%3A%20${addNumber($('#biaya_kirim').val(),true)}
      //           %0ASehingga%20total%20keseluruhan%20menjadi%3A%0A${addNumber((total+parseInt($('#biaya_kirim').val())),true)}
      //           %0A%0ADikirim%20ke%20alamat%3A
      //           %0A${response.alamat[0].provinsi_alamat}%20${response.alamat[0].kota_alamat}%20${response.alamat[0].kecamatan_alamat}%20${response.alamat[0].kelurahan_alamat}%20${response.alamat[0].kode_pos_alamat}%20 (${response.alamat[0].alamat_lengkap})%20
      //           %0A%0ASilahkan%20transfer%20senilai%20${addNumber((total+parseInt($('#biaya_kirim').val()+$('#kode_unik').val() )),true)}%2C%20ke%20salah%20satu%20rekening%20dibawah%20ini%3A%0A%0ABCA%0ANo.%20Rek%3A%20048%201425%20114%0AAtas%20Nama%3A%20Rakha%20Hendra%20Maryanto%0A%0AMandiri%0ANo.%20Rek%3A%2090000%202963%204269%0AAtas%20Nama%3A%20Rakha%20Hendra%20Maryanto%0A%0A%0ANB%3A%0A-%20Untuk%20mempercepat%20proses%20verifikasi%20harap%20transfer%20sesuai%20dengan%20nominal%0A-%20Setelah%20melakukan%20transfer%2C%20harap%20segera%20konfirmasi%20dengan%20mengirim%20bukti%20transfer%20melalui%20chat%20di%20WA%20ini.%0A%0ATerima%20kasih%20atas%20kepercayaan%20anda%20untuk%20belanja%20di%20toko%20kami%20%F0%9F%98%8A%0AKangenKampung.id`;
      window.open(
        `https://wa.me/${$('#phone').val()}?text=` + encodeURIComponent(first_text),
        '_blank' // <- This is what makes it open in a new window.
      );
    });

  }
</script>
<script>
  function change_status(id) {
    $('#modalDetail').modal("hide");
    var status = event.target.value;
    var form_data = new FormData();
    form_data.append('id_order', $('#id_order').val());
    form_data.append('kode_unik', $('#kode_unik').val());
    form_data.append('biaya_kirim', $('#biaya_kirim').val());
    form_data.append('no_resi', $('#no_resi').val());
    form_data.append('status', event.target.value);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    $.ajax({
      url: "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/change_status/",
      type: "POST",
      dataType: "json",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {
        $('#page-load').show();
        Swal.showLoading();
      },
      success: function(response) {
        Swal.hideLoading();
        if (response.result) {
          $("#phone").prop('disabled', true);
          $("#biaya_kirim").prop('disabled', true);
          $("#kode_unik").prop('disabled', true);
          message("Selamat", response.message.body, "success", "info", 1000);
          if (status == "2") {
            if (id == 1) {
              send_wa();
            } else {
              send_wa_tidak();
            }

          } else {
            setTimeout(function() {
              window.location = response.redirect
            }, 500);
          }
          // alert(status)
          detail_pembelian($('#id_order').val(), $('#id_member').val())
        } else {
          detail_pembelian($('#id_order').val(), $('#id_member').val())
          message("Mohon Maaf", response.message.body, "error", "info", 1000);
        }
      },
      error: function(request, status, error) {
        detail_pembelian($('#id_order').val(), $('#id_member').val())
        Swal.hideLoading();
        message("Mohon Maaf", "Silahkan Coba Kembali", "error", "info", 1000);
      }
    });
    // alert(event.target.value);
  }
</script>

<script>
  function detail_pembelian_tidak(id) {
    $('#id_order').val(id);
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/get_data_cart_tidak";
    var html = "";
    var pembayaran = "";
    var total = 0;
    var link = ``;
    var form_data = new FormData();
    form_data.append('id_order', id);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      $('#phone').val(response.order[0].tel_member);
      $('#nama').html(response.order[0].nama_member);
      $('#alamat').html(response.order[0].provinsi_alamat + " " + response.order[0].kota_alamat + " " + response.order[0].kecamatan_alamat + " " + response.order[0].kelurahan_alamat + " " + response.order[0].kode_pos_alamat + " <br> " + response.order[0].alamat_lengkap);
      for (i = 0; i < response.data.length; i++) {
        total += (parseInt(response.data[i].subtotal));
        html += `<tr>
                  <td>${response.data[i].name_product}</td>
                  <td>${response.data[i].qty}</td>
                  <td>${addNumber(response.data[i].subtotal,true)}</td>
            </tr>`;
      }
      $('#email').val(response.order[0].email_member);
      $(`#status_pemesanan option[value=${response.order[0].status_order}]`).attr('selected', 'selected');
      if (response.order[0].status_order == 4) {
        $("#no_resi").prop('disabled', false);
      } else {
        $("#no_resi").prop('disabled', true);
      }
      if (response.order[0].status_order > 1) {
        $("#phone").prop('disabled', true);
        $("#biaya_kirim").prop('disabled', true);
        $("#kode_unik").prop('disabled', true);
        $("#kode_unik").val(response.order[0].kode_unik);
        $("#biaya_kirim").val(response.order[0].ongkir);
        $("#no_resi").val(response.order[0].no_resi);
      } else {
        $("#no_resi").val(response.order[0].no_resi);
        $("#phone").prop('disabled', false);
        $("#biaya_kirim").prop('disabled', false);
        $("#kode_unik").prop('disabled', false);
        $('#kode_unik').val(Math.round(Math.random() * 100));
      }
      $('#data_cart').html(html);
      $('#total').html(addNumber(total, true));
      $('#modalDetail').modal("show");
      $("#status_pemesanan").change(function() {
        change_status(2);
      });
    });
  }
</script>

<script>
  function send_wa_tidak() {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>pemesanan/get_data_cart_tidak";
    var html = "";
    var pembayaran = "";
    var total = 0;
    var link = ``;
    var first_text = "";
    var form_data = new FormData();
    form_data.append('id_order', $('#id_order').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      first_text += `Yth. Bpk/Ibu *${response.order[0].nama_member}* ☺

Perkenankan, saya Fara dari *KangenKampung.id*, mau mengonfirmasi bahwa pesanan anda dengan rincian:

*No. Order*: ${$('#id_order').val()}
*Detail Barang*:
`;
      for (i = 0; i < response.data.length; i++) {
        total += (parseInt(response.data[i].subtotal));
        first_text += `-${response.data[i].qty} PCS ${response.data[i].name_product}
`;
      }
      first_text += `
Total belanja : ${addNumber(total,true)},-
Ongkos kirim : ${addNumber($('#biaya_kirim').val(),true)},-
Kode unik : ${addNumber($('#kode_unik').val(),true)},-
Sehingga total keseluruhan menjadi:
*${addNumber((total+(parseInt($('#biaya_kirim').val())+parseInt($('#kode_unik').val()))),true)}*,-

*Dikirim ke alamat:*
${response.order[0].provinsi_alamat + " " + response.order[0].kota_alamat + " " + response.order[0].kecamatan_alamat + " " + response.order[0].kelurahan_alamat + " " + response.order[0].kode_pos_alamat + " ( " + response.order[0].alamat_lengkap+" )"}

Silahkan transfer senilai *${addNumber((total+(parseInt($('#biaya_kirim').val())+parseInt($('#kode_unik').val()))),true)}*, ke salah satu rekening dibawah ini:

BCA
No. Rek: 048 1425 114
Atas Nama: Rakha Hendra Maryanto

Mandiri
No. Rek: 90000 2963 4269
Atas Nama: Rakha Hendra Maryanto


NB:
- Untuk mempercepat proses verifikasi harap transfer sesuai dengan nominal tertera sampai 3 digit angka terakhir
- Setelah melakukan transfer, harap segera konfirmasi dengan mengirim bukti transfer melalui chat di WA ini.

Terima kasih atas kepercayaan anda untuk belanja di toko kami 😊
*KangenKampung.id*`
      // link += ` %0ATotal%20belanja%20%3A%20${addNumber(total,true)}`;
      // link += `%0ADitambah%20ongkos%20kirim%20%3A%20${addNumber($('#biaya_kirim').val(),true)}
      //           %0ASehingga%20total%20keseluruhan%20menjadi%3A%0A${addNumber((total+parseInt($('#biaya_kirim').val())),true)}
      //           %0A%0ADikirim%20ke%20alamat%3A
      //           %0A${response.alamat[0].provinsi_alamat}%20${response.alamat[0].kota_alamat}%20${response.alamat[0].kecamatan_alamat}%20${response.alamat[0].kelurahan_alamat}%20${response.alamat[0].kode_pos_alamat}%20 (${response.alamat[0].alamat_lengkap})%20
      //           %0A%0ASilahkan%20transfer%20senilai%20${addNumber((total+parseInt($('#biaya_kirim').val()+$('#kode_unik').val() )),true)}%2C%20ke%20salah%20satu%20rekening%20dibawah%20ini%3A%0A%0ABCA%0ANo.%20Rek%3A%20048%201425%20114%0AAtas%20Nama%3A%20Rakha%20Hendra%20Maryanto%0A%0AMandiri%0ANo.%20Rek%3A%2090000%202963%204269%0AAtas%20Nama%3A%20Rakha%20Hendra%20Maryanto%0A%0A%0ANB%3A%0A-%20Untuk%20mempercepat%20proses%20verifikasi%20harap%20transfer%20sesuai%20dengan%20nominal%0A-%20Setelah%20melakukan%20transfer%2C%20harap%20segera%20konfirmasi%20dengan%20mengirim%20bukti%20transfer%20melalui%20chat%20di%20WA%20ini.%0A%0ATerima%20kasih%20atas%20kepercayaan%20anda%20untuk%20belanja%20di%20toko%20kami%20%F0%9F%98%8A%0AKangenKampung.id`;

      window.open(
        `https://wa.me/${$('#phone').val()}?text=` + encodeURIComponent(first_text),
        '_blank' // <- This is what makes it open in a new window.
      );
    });

  }

  function chat(no) {
    window.open(
      `https://wa.me/${no}?text=Hai`,
      '_blank' // <- This is what makes it open in a new window.
    );
  }
</script>