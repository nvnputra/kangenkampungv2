<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pemesanan extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_Pemesanan');
		$this->load->library('recaptcha');
	}

	public function index()
	{
		// if(\strpos(get_role()->role, "4")){
		$data['content'] = 'Pemesanan';
		$data['page_active'] = 'Pemesanan';
		$this->load->view('template', $data);
		// }else{
		//     echo "Tidak Ada Akses";         
		// }   	

	}

	function ajax_action_datatable_terdaftar()
	{
		$column = "*";
		$table = "m__order a";
		$column_order = array('id_order', 'name_member', 'status_order');
		$column_search = array('id_order', 'name_member', 'status_order');
		$order = array('a.creted_at' => 'DESC');
		$where = '';
		$joins = array(
			array(
				'table' => 'm__member b',
				'condition' => 'a.id_member = b.id_member',
				'jointype' => ''
			),
		);
		$list = $this->M_Pemesanan->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);

		$link = '' . base_url() . $this->config->item('index_page') . '/order';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			$row = array();
			if ($key->status_order == 1) {
				$text = "belum Di Cek";
				$btn = "btn btn-warning";
			} else if ($key->status_order == 2) {
				$text = "Telah Dikirim";
				$btn = "btn btn-info";
			} else if ($key->status_order == 3) {
				$text = "Bukti Di Upload";
				$btn = "btn btn-info";
			} else if ($key->status_order == 4) {
				$text = "Berhasil";
				$btn = "btn btn-info";
			} else if ($key->status_order == 5) {
				$text = "Barang Dikirim";
				$btn = "btn btn-info";
			} else if ($key->status_order == 6) {
				$text = "Barang Sampai";
				$btn = "btn btn-success";
			} else if ($key->status_order == 7) {
				$text = "Pesanan Batal";
				$btn = "btn btn-danger";
			}
			$row[] = $key->id_order;
			$row[] = $key->name_member;
			// $row[] = $key->no_member;
			$row[] = "<input type='button' class='" . $btn . "' value='" . $text . "' onClick='detail_pembelian(" . "\"" . $key->id_order . "\"," . "\"" . $key->id_member . "\"" . ")'/>
			<button  class='btn btn-success'  onClick='chat(" . $key->no_member . ")'>Chat</button>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_Pemesanan->count_all($table, $where, $joins),
			"recordsFiltered" => $this->M_Pemesanan->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
			"data" => $data,
		);

		echo json_encode($output);
	}

	function ajax_action_datatable_tidak_terdaftar()
	{
		$column = "*";
		$table = "m__order a";
		$column_order = array('id_order', 'name_member', 'status_order');
		$column_search = array('id_order', 'name_member', 'status_order');
		$order = array('a.creted_at' => 'DESC');
		$where = 'id_member = 0';
		$joins = '';
		$list = $this->M_Pemesanan->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);

		$link = '' . base_url() . $this->config->item('index_page') . '/order';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			$row = array();

			if ($key->status_order == 1) {
				$text = "belum Di Cek";
				$btn = "btn btn-warning";
			} else if ($key->status_order == 2) {
				$text = "Telah Dikirim";
				$btn = "btn btn-info";
			} else if ($key->status_order == 3) {
				$text = "Bukti Di Upload";
				$btn = "btn btn-info";
			} else if ($key->status_order == 4) {
				$text = "Berhasil";
				$btn = "btn btn-info";
			} else if ($key->status_order == 5) {
				$text = "Barang Dikirim";
				$btn = "btn btn-info";
			} else if ($key->status_order == 6) {
				$text = "Barang Sampai";
				$btn = "btn btn-success";
			} else if ($key->status_order == 7) {
				$text = "Pesanan Batal";
				$btn = "btn btn-danger";
			}

			$row[] = $key->id_order;
			$row[] = $key->nama_member;
			// $row[] = $key->tel_member;
			$row[] = "<input type='button' class='" . $btn . "' value='" . $text . "' onClick='detail_pembelian_tidak(" . "\"" . $key->id_order . "\"" . ")'/>
			<button  class='btn btn-success'  onClick='chat(" . $key->tel_member . ")'>Chat</button>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_Pemesanan->count_all($table, $where, $joins),
			"recordsFiltered" => $this->M_Pemesanan->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
			"data" => $data,
		);

		echo json_encode($output);
	}

	public function get_data_cart()
	{
		$id = post("id_order");
		$id_member = post("id_member");
		$joins = array(
			array(
				'table' => 'mr__product b',
				'condition' => 'a.id_product = b.id_product',
				'jointype' => ''
			),
		);
		$dataproduk = $this->M_Pemesanan->fetch_joins('m__detail_order a ', '*', $joins, 'a.id_order = ' . "'" . $id . "'", '', '', TRUE);
		$order = $this->M_Pemesanan->fetch_joins('m__order a ', '*', "", 'a.id_order = ' . "'" . $id . "'", '', '', TRUE);
		$datamember = $this->M_Pemesanan->fetch_joins('m__member ', '*', '', 'id_member = ' . "'" . $id_member . "'", '', '', TRUE);
		$dataalamat = $this->M_Pemesanan->fetch_joins('m__alamat ', '*', '', 'id_member = ' . "'" . $id_member . "'", '', '', TRUE);
		// print_r($dataalamat);
		if (count($dataproduk) <= 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses '),
				"form_error" => '',
				"data" => $dataproduk,
				"member" => $datamember,
				"alamat" => $dataalamat,
				"order" => $order
				// "redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
			);
			print json_encode($json_data);
		}
	}

	public function get_data_cart_tidak()
	{
		$id = post("id_order");
		$joins = array(
			array(
				'table' => 'mr__product b',
				'condition' => 'a.id_product = b.id_product',
				'jointype' => ''
			),
		);
		$dataproduk = $this->M_Pemesanan->fetch_joins('m__detail_order a ', '*', $joins, 'a.id_order = ' . "'" . $id . "'", '', '', TRUE);
		$order = $this->M_Pemesanan->fetch_joins('m__order a ', '*', "", 'a.id_order = ' . "'" . $id . "'", '', '', TRUE);
		// print_r($dataalamat);
		if (count($dataproduk) <= 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses '),
				"form_error" => '',
				"data" => $dataproduk,
				"order" => $order
				// "redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
			);
			print json_encode($json_data);
		}
	}

	public function change_status()
	{
		$id = post('id_order');
		$status = post('status');
		$resi = post('resi');
		if ($status == 2) {
			$data = array(
				"status_order" => $status,
				"kode_unik" => post('kode_unik'),
				"ongkir" => post('biaya_kirim')
			);
		} else if ($status == 5) {
			$data = array(
				"status_order" => $status,
				"no_resi" => post('no_resi')
			);
		} else {
			$data = array(
				"status_order" => $status
			);
		}

		$edit = $this->M_Pemesanan->update_table('m__order', $data, 'id_order', $id);
		$this->db->trans_complete();
		if ($edit == FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Merubah Status Pemesanan'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses Merubah Status Pemesanan'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . '/pemesanan'
			);
			print json_encode($json_data);
		}
	}
}
