<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_account extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_paymentaccount');
		$this->load->library('recaptcha');
	}

	public function index()
	{
		$data['content'] = 'payment_account';
		$data['page_active'] = 'payment_account';
		$this->load->view('template', $data);
	}

	public function ajax_action_add_payment_account()
	{
		$this->form_validation->set_rules('id_payment_method', 'id_payment_method', 'required');
		$this->form_validation->set_rules('name_payment_account', 'name_payment_account', 'required');
		$this->form_validation->set_rules('nomor_payment_account', 'nomor_payment_account', 'required');


		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$data = array(
				"id_payment_method" => post("id_payment_method"),
				"name_payment_account" => post("name_payment_account"),
				"nomor_payment_account" => post("nomor_payment_account"),
				"status_payment_account" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_paymentaccount->insert_table("a__payment_account", $data);
			if ($add == FALSE) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . '/payment_account'
				);
				print json_encode($json_data);
			}
		}
	}

	function ajax_action_datatable_payment_account()
	{
		$column = "*";
		$table = "a__payment_account a";
		$column_order = array('name_payment_account', 'nomor_payment_account', 'type_payment_account', 'created_at');
		$column_search = array('name_payment_account', 'nomor_payment_account', 'type_payment_account', 'created_at');
		$order = array('id_payment_account' => 'DESC');
		$where = "a.status_payment_account != 3";
		$joins = array(
			array(
				'table' => 'a__payment_method b',
				'condition' => 'a.id_payment_method = b.id_payment_method',
				'jointype' => ''
			),
		);
		$list = $this->M_paymentaccount->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);

		$link = '' . base_url() . $this->config->item('index_page') . '/payment_account';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			if ($key->status_payment_account == 1) {
				$status = "Active";
			} else {
				$status = "Hidden";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->name_payment_account;
			$row[] = $key->name_payment_method;
			$row[] = $key->nomor_payment_account;
			$row[] = $status;
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete(' . "'" . $link . "'" . ',' . "'ajax_action_delete_payment_account'" . ',' . $key->id_payment_account . ')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal(' . $key->id_payment_account . ')" value="Edit">';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_paymentaccount->count_all($table, $where, $joins),
			"recordsFiltered" => $this->M_paymentaccount->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
			"data" => $data,
		);

		echo json_encode($output);
	}

	function ajax_action_get_payment_account()
	{
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'a__payment_method b',
				'condition' => 'a.id_payment_method = b.id_payment_method',
				'jointype' => ''
			),
		);
		$data = $this->M_paymentaccount->fetch_joins('a__payment_account a', '*', $joins, 'id_payment_account = ' . $id, '', TRUE);

		if (count($data) == 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_delete_payment_account()
	{
		$id = $this->uri->segment(3);
		$data = array(
			"status_payment_account" => 3
		);
		$edit = $this->M_paymentaccount->update_table('a__payment_account', $data, 'id_payment_account', $id);
		$this->db->trans_complete();
		if ($edit == FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . '/payment_account'
			);
			print json_encode($json_data);
		}
	}

	function ajax_action_edit()
	{
		$this->form_validation->set_rules('id_payment_method', 'id_payment_method', 'required');
		$this->form_validation->set_rules('name_payment_account', 'name_payment_account', 'required');
		$this->form_validation->set_rules('nomor_payment_account', 'nomor_payment_account', 'required');


		### FORM VALIDATION
		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}
		$id = $this->uri->segment(3);

		### TRANSACTION START
		$this->db->trans_start();
		$data = array(
			"id_payment_method" => post("id_payment_method"),
			"name_payment_account" => post("name_payment_account"),
			"nomor_payment_account" => post("nomor_payment_account"),
			"update_at" => date("Y-m-d H:i:s")
		);

		$edit = $this->M_paymentaccount->update_table('a__payment_account', $data, 'id_payment_account', post('id_payment_account'));

		$this->db->trans_complete();
		### TRANSACTION END

		if ($edit == FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal update Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses update Data'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . '/payment_account/'
			);
			print json_encode($json_data);
		}
	}
}
