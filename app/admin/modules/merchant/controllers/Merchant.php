<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Merchant extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_merchant');
	}
	public function index()
	{

		$data['content'] = 'merchant';
		$this->load->view('template', $data);
	}

	public function ajax_action_add_merchant()
	{
		$this->form_validation->set_rules('name_merchant', 'name_merchant', 'required');
		$this->form_validation->set_rules('owner_merchant', 'owner_merchant', 'required');

		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$count_data_detail = $this->M_merchant->get_count_data('mr__merchant_detail');
			$id_merchant_detail = "MRD" . date('his') . $count_data_detail;
			$data_two = array(
				"id_detail_merchant" => $id_merchant_detail,
				"name_provinsi_merchant" => post("name_provinsi"),
				"name_kota_merchant" => post("name_kota"),
				"name_kecamatan_merchant" => post("name_kecamatan"),
				"name_kelurahan_merchant" => post("kelurahan"),
				"id_provinsi" => post("id_provinsi"),
				"id_kota" => post("id_kota"),
				"id_kecamatan" => post("id_kecamatan"),
				"id_kelurahan" => post("id_kelurahan"),
				"kode_pos_merchant" => post("kode_pos"),
				"keterangan" => post("keterangan"),
				"created_at" => date("Y-m-d H:i:s")
			);
			$add_two = $this->M_merchant->insert_table("mr__merchant_detail", $data_two);
			$last_data = $this->M_merchant->get_last_data('id_detail_merchant', 'mr__merchant_detail', 'DESC');
			$count_data = $this->M_merchant->get_count_data('mr__merchant');
			$id_merchant = "MR" . date('his') . $count_data;
			$data = array(
				"id_merchant" => $id_merchant,
				"name_merchant" => post("name_merchant"),
				"no_merchant" => post("no_merchant"),
				"email_merchant" => post("email_merchant"),
				"id_detail_merchant" => $last_data->id_detail_merchant,
				"owner_merchant" => post("owner_merchant"),
				"status_merchant" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_merchant->insert_table("mr__merchant", $data);
			if ($add == FALSE) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . 'merchant'
				);
				print json_encode($json_data);
			}
		}
	}

	public function ajax_action_edit_merchant()
	{
		$this->form_validation->set_rules('id_merchant', 'id_merchant', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$id_detail_merchant = $this->M_merchant->get_row("id_detail_merchant", "mr__merchant", "id_merchant =" + post('id_merchant'), '', '', TRUE);
			$data_two = array(
				"name_provinsi_merchant" => post("name_provinsi"),
				"name_kota_merchant" => post("name_kota"),
				"name_kecamatan_merchant" => post("name_kecamatan"),
				"name_kelurahan_merchant" => post("kelurahan"),
				"id_provinsi" => post("id_provinsi"),
				"id_kota" => post("id_kota"),
				"id_kecamatan" => post("id_kecamatan"),
				"id_kelurahan" => post("id_kelurahan"),
				"kode_pos_merchant" => post("kode_pos"),
				"keterangan" => post("keterangan"),
				"update_at" => date("Y-m-d H:i:s")
			);
			$add_two = $this->M_merchant->update_table("mr__merchant_detail", $data_two, "id_detail_merchant", $id_detail_merchant['id_detail_merchant']);

			$data = array(
				"name_merchant" => post("name_merchant"),
				"no_merchant" => post("no_merchant"),
				"email_merchant" => post("email_merchant"),
				"owner_merchant" => post("owner_merchant"),
				"status_merchant" => 1,
				"update_at" => date("Y-m-d H:i:s")
			);
			$edit = $this->M_merchant->update_table("mr__merchant", $data, "id_merchant", post("id_merchant"));
			if ($edit == FALSE) {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Failed', 'body' => 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . '/merchant'
				);
				print json_encode($json_data);
			}
		}
	}

	function ajax_action_merchant()
	{
		$id = $this->uri->segment(3);
		$joins = array(
			array(
				'table' => 'mr__merchant_detail b',
				'condition' => 'a.id_detail_merchant = b.id_detail_merchant',
				'jointype' => ''
			),
		);
		$data = $this->M_merchant->fetch_joins('mr__merchant a', '*', $joins, 'id_merchant = \'' . $id . '\'', '', TRUE);

		if (count($data) == 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_list_merchant()
	{
		$column = "a.*,b.*";
		$column_order = array('owner_merchant', 'name_merchant');
		$column_search = array('owner_merchant', 'name_merchant');
		$order = array('id_merchant' => 'DESC');
		$table = "mr__merchant a";
		$where = "";
		$joins = array(
			array(
				'table' => 'mr__merchant_detail b',
				'condition' => 'a.id_detail_merchant = b.id_detail_merchant',
				'jointype' => ''
			),
		);
		$list = $this->M_merchant->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);
		$data = array();
		$link = '' . base_url() . $this->config->item('index_page') . '/merchant';
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			if ($key->status_merchant == 1) {
				$status = "<badge class='badge badge-success'>Aktif</badge>";
			} else {
				$status = "<badge class='badge badge-danger'>Disable</badge>";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->name_merchant;
			$row[] = $key->no_merchant;
			$row[] = $key->email_merchant;
			$row[] = $key->name_kota_merchant;
			$row[] = $status;
			$row[] = '<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal(\'' . $key->id_merchant . '\')" value="Edit">';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_merchant->count_all($table, $where, $joins),
			"recordsFiltered" => $this->M_merchant->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function ajax_action_get_all()
	{
		$data = $this->M_merchant->fetch_joins('mr__merchant a', '*', "", '', '', TRUE);

		if (count($data) == 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}
}
