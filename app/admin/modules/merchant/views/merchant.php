<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Daftar Merchant</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-0">Tambah Merchant</h3>
                </div>
                <div class="col-6" style="text-align: right">
                    <button class="btn btn-primary" type="submit" id="show-merchant"><i class="fas fa-chevron-down"></i></button>
                </div>
            </div>

        </div>
        <!-- Card body -->
        <div class="card-body" id="form-add-merchant" style="display: none">
            <!-- Form groups used in grid -->
            <div class="card-body">
                <!-- Form groups used in grid -->
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols1Input">Nama Toko</label>
                            <input type="text" class="form-control" id="name_merchant" placeholder="Nama Toko">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols2Input">Pemilik</label>
                            <input type="text" class="form-control" id="owner_merchant" placeholder="Owner Pemilik">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols3Input">Email</label>
                            <input type="text" class="form-control" id="email_merchant" placeholder="Email Merchant">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols3Input">No Handphone(WA)</label>
                            <input type="text" class="form-control" id="no_merchant" placeholder="No Handphone">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example4cols1Input">Provinsi</label>
                            <select class="form-control" id="provinsi">

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example4cols1Input">Kota</label>
                            <select class="form-control" id="kota">

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example4cols1Input">Kecamatan</label>
                            <select class="form-control" id="kecamatan">

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="form-control-label" for="example4cols1Input">Kelurahan</label>
                            <input type="text" class="form-control" id="kelurahan" placeholder="kelurahan">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-control-label" for="example2cols1Input">Kode Pos</label>
                            <input type="text" class="form-control" id="kode_pos" placeholder="One of two cols">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-control-label" for="example2cols2Input">Keterangan (Alamat Lengkap)</label>
                            <textarea id="keterangan" class="form-control">

                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align: right">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" onclick="ajax_action_add_merchant()">Tambah Merchant</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <h3 class="mb-0">Data toko</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-merchant">Edit Kategori</button> -->
            <div class="table-responsive py-4">
                <table class="table" id="list_merchant">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">No</th>
                            <th scope="col">Email</th>
                            <th scope="col">Kota</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal edit -->

<!-- Modal -->
<div id="edit-merchant" class="modal fade" role="dialog">
    <div class="modal-dialog modal-centered modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="card-wrapper">
                <!-- Input groups -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Ganti Merchant</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3" style="display: none">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols1Input">Nama Toko</label>
                                    <input type="text" class="form-control" id="edit_id_merchant" placeholder="Nama Toko">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols1Input">Nama Toko</label>
                                    <input type="text" class="form-control" id="edit_name_merchant" placeholder="Nama Toko">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols2Input">Pemilik</label>
                                    <input type="text" class="form-control" id="edit_owner_merchant" placeholder="Owner Pemilik">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols3Input">Email</label>
                                    <input type="text" class="form-control" id="edit_email_merchant" placeholder="Email Merchant">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols3Input">No Handphone(WA)</label>
                                    <input type="text" class="form-control" id="edit_no_merchant" placeholder="No Handphone">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example4cols1Input">Provinsi</label>
                                    <select class="form-control" id="edit_provinsi">

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example4cols1Input">Kota</label>
                                    <select class="form-control" id="edit_kota">

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example4cols1Input">Kecamatan</label>
                                    <select class="form-control" id="edit_kecamatan">

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="example4cols1Input">Kelurahan</label>
                                    <input type="text" class="form-control" id="edit_kelurahan" placeholder="kelurahan">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="example2cols1Input">Kode Pos</label>
                                    <input type="text" class="form-control" id="edit_kode_pos" placeholder="One of two cols">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="example2cols2Input">Keterangan (Alamat Lengkap)</label>
                                    <textarea id="edit_keterangan" class="form-control">

                            </textarea>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: right">
                            <button class="btn btn-primary" type="submit" onclick="ajax_action_edit_merchant()">Ganti Kategori</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<?php include("js/js.php"); ?>