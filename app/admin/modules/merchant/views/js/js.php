<script type="text/javascript">
  $(document).ready(function() {
    set_provinci();
    $('#show-merchant').click(function() {
      $('#form-add-merchant').toggle(1000);
    });
  });
</script>
<!--  -->
<script type="text/javascript">
  function ajax_action_add_merchant() {
    var form_data = new FormData();
    form_data.append('name_merchant', $('#name_merchant').val());
    form_data.append('no_merchant', $('#no_merchant').val());
    form_data.append('email_merchant', $('#email_merchant').val());
    form_data.append('owner_merchant', $('#owner_merchant').val());
    form_data.append('id_provinsi', $('#provinsi :selected').val());
    form_data.append('name_provinsi', $('#provinsi :selected').text());
    form_data.append('id_kota', $('#kota :selected').val());
    form_data.append('name_kota', $('#kota :selected').text());
    form_data.append('id_kecamatan', $('#kecamatan :selected').val());
    form_data.append('name_kecamatan', $('#kecamatan :selected').text());
    form_data.append('alamat_lengkap_merchant', $('#alamat_lengkap_merchant').val());
    form_data.append('kelurahan', $('#kelurahan').val());
    form_data.append('kode_pos', $('#kode_pos').val());
    form_data.append('keterangan', $('#keterangan').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/merchant/ajax_action_add_merchant/", "POST", form_data);
  }

  function ajax_action_edit_merchant() {
    var form_data = new FormData();
    form_data.append('name_merchant', $('#edit_name_merchant').val());
    form_data.append('no_merchant', $('#edit_no_merchant').val());
    form_data.append('email_merchant', $('#edit_email_merchant').val());
    form_data.append('owner_merchant', $('#edit_owner_merchant').val());
    form_data.append('id_provinsi', $('#edit_provinsi :selected').val());
    form_data.append('name_provinsi', $('#edit_provinsi :selected').text());
    form_data.append('id_kota', $('#edit_kota :selected').val());
    form_data.append('name_kota', $('#edit_kota :selected').text());
    form_data.append('id_kecamatan', $('#edit_kecamatan :selected').val());
    form_data.append('name_kecamatan', $('#edit_kecamatan :selected').text());
    form_data.append('alamat_lengkap_merchant', $('#edit_alamat_lengkap_merchant').val());
    form_data.append('kelurahan', $('#edit_kelurahan').val());
    form_data.append('kode_pos', $('#edit_kode_pos').val());
    form_data.append('keterangan', $('#edit_keterangan').val());
    form_data.append('id_merchant', $('#edit_id_merchant').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/merchant/ajax_action_edit_merchant/", "POST", form_data);
    $('#edit-merchant').modal("hide");
  }
</script>

<!--call -->
<script type="text/javascript">
  function ajax_show_edit_modal(id) {
    var link = "<?php echo base_url() . $this->config->item('index_page'); ?>merchant/ajax_action_merchant/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');

    ajaxShowData(link, "POST", form_data, function(response) {
      $('#edit_name_merchant').val(response.data[0].name_merchant);
      $('#edit_email_merchant').val(response.data[0].email_merchant);
      $('#edit_owner_merchant').val(response.data[0].owner_merchant);
      $('#edit_no_merchant').val(response.data[0].no_merchant);
      // $('#edit_provinsi_merchant').val(response.data[0].provinsi_merchant);
      // $('#edit_kota_merchant').val(response.data[0].kota_merchant);
      // $('#edit_kecamatan_merchant').val(response.data[0].kecamatan_merchant);
      $('#edit_kelurahan').val(response.data[0].name_kelurahan_merchant);
      $('#edit_kode_pos').val(response.data[0].kode_pos_merchant);
      $('#edit_keterangan').val(response.data[0].keterangan);
      $('#edit_id_merchant').val(response.data[0].id_merchant);
      edit_set_provinci(response.data[0].id_provinsi, response.data[0].name_provinsi_merchant);
      set_kota_detail(response.data[0].id_provinsi, response.data[0].id_kota, response.data[0].name_kota_merchant);
      set_kecamatan_detail(response.data[0].id_kota, response.data[0].id_kecamatan, response.data[0].name_kecamatan_merchant)
      $('#page-load').hide();
    });
    $('#edit-merchant').modal("show");

  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    dataTableShow("#list_merchant", "<?php echo base_url() . $this->config->item('index_page'); ?>merchant/ajax_list_merchant", form_data);
  });
</script>
<script type="text/javascript">
  function set_provinci() {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    form_data.append('param', "province");
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      var html = '<option value="" selected="selected">Pilih Provinsi</option>';
      var i;
      for (i = 0; i < response.rajaongkir.results.length; i++) {
        html += "<option value='" + response.rajaongkir.results[i].province_id + "'>" + response.rajaongkir.results[i].province + "</option>";
      }
      $('#provinsi').html(html);
      $('#page-load').hide();
    });
  }
  $('#provinsi').on('change', function() {
    set_kota_detail(this.value);
    $('#page-load').hide();
  });

  function set_kota_detail(value) {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    form_data.append('param', "city?province=" + value);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      ;
      var html = '<option value="" selected="selected">Pilih Kota</option>';
      var i;
      for (i = 0; i < response.rajaongkir.results.length; i++) {
        html += "<option value='" + response.rajaongkir.results[i].city_id + "'>" + response.rajaongkir.results[i].city_name + "</option>";
      }
      $('#kota').html(html);
      $('#page-load').hide();
    });
  }
  $('#kota').on('change', function() {
    set_kecamatan_detail(this.value);
    $('#page-load').hide();
  });

  function set_kecamatan_detail(value) {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    form_data.append('param', "subdistrict?city=" + value);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      ;
      var html = '<option value="" selected="selected">Pilih Kecamatan</option>';
      var i;
      for (i = 0; i < response.rajaongkir.results.length; i++) {
        html += "<option value='" + response.rajaongkir.results[i].subdistrict_id + "'>" + response.rajaongkir.results[i].subdistrict_name + "</option>";
      }
      $('#kecamatan').html(html);
      $('#page-load').hide();
    });
  }
</script>



<script type="text/javascript">
  function edit_set_provinci(id, name) {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    form_data.append('param', "province");
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      var html = '<option value="' + id + '" selected="selected">' + name + '</option>';
      var i;
      for (i = 0; i < response.rajaongkir.results.length; i++) {
        html += "<option value='" + response.rajaongkir.results[i].province_id + "'>" + response.rajaongkir.results[i].province + "</option>";
      }
      $('#edit_provinsi').html(html);
      $('#page-load').hide();
    });
  }
  $('#edit_provinsi').on('change', function() {
    edit_set_kota_detail(this.value, '0', "Pilih Kota");
    $('#page-load').hide();
  });

  function edit_set_kota_detail(value, id, name) {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    form_data.append('param', "city?province=" + value);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      ;
      var html = '<option value="' + id + '" selected="selected">' + name + '</option>';
      var i;
      for (i = 0; i < response.rajaongkir.results.length; i++) {
        html += "<option value='" + response.rajaongkir.results[i].city_id + "'>" + response.rajaongkir.results[i].city_name + "</option>";
      }
      $('#edit_kota').html(html);
      $('#page-load').hide();
    });
  }
  $('#edit_kota').on('change', function() {
    edit_set_kecamatan_detail(this.value, '0', "Pilih Kecamatan");
    $('#page-load').hide();
  });

  function edit_set_kecamatan_detail(value, id, name) {
    var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
    $('#page-load').show();
    var form_data = new FormData();
    form_data.append('param', "subdistrict?city=" + value);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    ajaxShowData(link2, "POST", form_data, function(response) {
      ;
      var html = '<option value="' + id + '" selected="selected">' + name + '</option>';
      var i;
      for (i = 0; i < response.rajaongkir.results.length; i++) {
        html += "<option value='" + response.rajaongkir.results[i].subdistrict_id + "'>" + response.rajaongkir.results[i].subdistrict_name + "</option>";
      }
      $('#edit_kecamatan').html(html);
      $('#page-load').hide();
    });
  }
</script>