<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Daftar Metode Pembayaran</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-0">Tambah Metode Pembayaran</h3>
                </div>
                <div class="col-6" style="text-align: right">
                    <button class="btn btn-primary" type="submit" id="show-metode"><i class="fas fa-chevron-down"></i></button>
                </div>
            </div>

        </div>
        <!-- Card body -->
        <div class="card-body" id="form-add-metode" style="display: none">
            <!-- Form groups used in grid -->
            <div class="card-body">
                <!-- Form groups used in grid -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols1Input">Nama Bank</label>
                            <input type="text" class="form-control" id="name_payment_method" placeholder="Nama Bank">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols2Input">Code Bank</label>
                            <input type="text" class="form-control" id="code_payment_method" placeholder="Owner Code Bank">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label" for="example3cols3Input">Catatan Bank</label>
                            <textarea id="desc_payment" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align: right">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" onclick="ajax_action_add_payment_method()">Tambah Metode</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <h3 class="mb-0">Data Metode Pembayaran</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-merchant">Edit Kategori</button> -->
            <div class="table-responsive py-4">
                <table class="table" id="list_payment_method">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Code</th>
                            <th scope="col">Catatan</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal edit -->

<!-- Modal -->
<div id="edit-methode" class="modal fade" role="dialog">
    <div class="modal-dialog modal-centered modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="card-wrapper">
                <!-- Input groups -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Ganti Metode Pembayaran</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols1Input">Nama Bank</label>
                                    <input type="text" class="form-control" id="edit_name_payment_method" placeholder="Nama Bank">
                                    <input type="text" class="form-control" id="edit_id_payment_method" placeholder="Nama Bank" style="display: none">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols2Input">Code Bank</label>
                                    <input type="text" class="form-control" id="edit_code_payment_method" placeholder="Owner Code Bank">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols3Input">Catatan Bank</label>
                                    <textarea id="edit_desc_payment" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: right">
                            <button class="btn btn-primary" type="submit" onclick="ajax_action_edit_payment_method()">Ganti Metode Pembayaran</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<?php include("js/js.php") ?>