<script type="text/javascript">
  $(document).ready(function() {
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    dataTableShow("#list_payment_method", "<?php echo base_url() . $this->config->item('index_page'); ?>payment_method/ajax_list_payment_method", form_data);
    $('#show-metode').click(function() {
      $('#form-add-metode').toggle(1000);
    });
  });
</script>

<script type="text/javascript">
  function ajax_action_add_payment_method() {
    var catatan_method = $('textarea#desc_payment').val();
    var form_data = new FormData();
    form_data.append('name_payment_method', $('#name_payment_method').val());
    form_data.append('code_payment_method', $('#code_payment_method').val());
    form_data.append('catatan_method', catatan_method);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/payment_method/ajax_action_add_payment/", "POST", form_data);
  }

  function ajax_show_edit_modal(id) {
    var link = "<?php echo base_url() . $this->config->item('index_page'); ?>payment_method/ajax_action_get_payment_method/" + id;
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');

    ajaxShowData(link, "POST", form_data, function(response) {
      var name_payment_method = $('#edit_name_payment_method').val(response.data[0].name_payment_method);
      var code_payment_method = $('#edit_code_payment_method').val(response.data[0].code_payment_method);
      var id_payment_method = $('#edit_id_payment_method').val(response.data[0].id_payment_method);
      var edit_catatan = $('#edit_desc_payment').val(response.data[0].catatan_method);
      $('#edit_catatan_method').html(response.data[0].catatan_method);


      $('#page-load').hide();
    });
    $('#edit-methode').modal("show");

  }

  function ajax_action_edit_payment_method() {
    var catatan_method = $('textarea#edit_desc_payment').val();
    // var file_data = $('#edit_attachment_payment_method').prop('files')[0];
    var form_data = new FormData();
    form_data.append('name_payment_method', $('#edit_name_payment_method').val());
    form_data.append('code_payment_method', $('#edit_code_payment_method').val());
    form_data.append('id_payment_method', $('#edit_id_payment_method').val());
    form_data.append('catatan_method', catatan_method);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>payment_method/ajax_action_edit_payment_method/", "POST", form_data);
    $('#edit-methode').modal("hide");
  }
</script>