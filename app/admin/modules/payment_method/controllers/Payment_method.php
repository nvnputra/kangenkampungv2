<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_method extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_paymentmethod');
		$this->load->library('recaptcha');
	}

	public function index()
	{
		$data['content'] = 'payment_method';
		$data['page_active'] = 'payment_method';
		$this->load->view('template', $data);
	}

	public function ajax_action_add_payment()
	{
		$this->form_validation->set_rules('name_payment_method', 'name_payment_method', 'required');
		// $this->form_validation->set_rules('api_payment_method', 'api_payment_method', 'required');
		$this->form_validation->set_rules('code_payment_method', 'code_payment_method', 'required');

		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$data = array(
				"name_payment_method" => post("name_payment_method"),
				"code_payment_method" => post("code_payment_method"),
				"catatan_method" => post("catatan_method"),
				"status_payment_method" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_paymentmethod->insert_table("a__payment_method", $data);
			if ($add == FALSE) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . '/payment_method'
				);
				print json_encode($json_data);
			}
		}
	}

	public function ajax_list_payment_method()
	{
		$column = "a.*";
		$column_order = array('name_payment_method', 'code_payment_method', 'api_payment_method');
		$column_search = array('name_payment_method', 'code_payment_method', 'api_payment_method');
		$order = array('id_payment_method' => 'DESC');
		$table = "a__payment_method a";
		$where = "a.status_payment_method != 3";
		$joins = "";
		$list = $this->M_paymentmethod->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);
		$data = array();
		$link = '' . base_url() . $this->config->item('index_page') . '/payment_method';
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			if ($key->status_payment_method == 1) {
				$status = "Active";
			} else {
				$status = "Hidden";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->name_payment_method;
			$row[] = $key->code_payment_method;
			$row[] = $key->catatan_method;
			$row[] = $status;
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete(' . "'" . $link . "'" . ',' . "'ajax_action_delete_payment_method'" . ',' . $key->id_payment_method . ')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal(' . $key->id_payment_method . ')" value="Edit">';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_paymentmethod->count_all($table, $where, $joins),
			"recordsFiltered" => $this->M_paymentmethod->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
			"data" => $data,
		);
		echo json_encode($output);
	}

	function ajax_action_get_payment_method()
	{
		$id = $this->uri->segment(3);
		$joins = "";
		$data = $this->M_paymentmethod->fetch_joins('a__payment_method a', '*', $joins, 'id_payment_method = ' . $id, '', TRUE);

		if (count($data) == 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	function ajax_action_get_payment_method_all()
	{
		$data = $this->M_paymentmethod->fetch_joins('a__payment_method a', '*', '', '', '', TRUE);

		if (count($data) == 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_delete_payment_method()
	{
		$id = $this->uri->segment(3);
		$data = array(
			"status_payment_method" => 3
		);
		$edit = $this->M_paymentmethod->update_table('a__payment_method', $data, 'id_payment_method', $id);
		$this->db->trans_complete();
		if ($edit == FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . 'payment_method'
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_edit_payment_method()
	{
		$this->form_validation->set_rules('name_payment_method', 'name_payment_method', 'required');
		// $this->form_validation->set_rules('api_payment_method', 'api_payment_method', 'required');
		$this->form_validation->set_rules('code_payment_method', 'code_payment_method', 'required');

		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {

			$data = array(
				"name_payment_method" => post("name_payment_method"),
				"code_payment_method" => post("code_payment_method"),
				"catatan_method" => post("catatan_method"),
				"status_payment_method" => 1,
				"update_at" => date("Y-m-d H:i:s")
			);


			$edit = $this->M_paymentmethod->update_table("a__payment_method", $data, "id_payment_method", post("id_payment_method"));
			if ($edit == FALSE) {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Failed', 'body' => 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengedit data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . 'payment_method'
				);
				print json_encode($json_data);
			}
		}
	}
}
