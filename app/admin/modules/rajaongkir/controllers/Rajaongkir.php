<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rajaongkir extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_Ongkir');
	}

	public function get_provinci_ongkir()
	{
		$param = post('param');
		$get = $this->ongkir->get_kurir_data($param);
		echo $get;
	}


	public function get_ongkir()
	{
		$origin = 'origin=' . post('origin');
		$origintype = '&originType=subdistrict';
		$destination = '&destination=' . post('destination');
		$destinationtype = '&destinationType=subdistrict';
		$weight = '&weight=' . post('weight');
		$kurir = '&courier=' . post('kurir');
		$diameter = '&diameter=' . post('diameter');
		$get = $this->ongkir->cek_ongkir($origin . $origintype . $destination . $destinationtype . $weight . $kurir . $diameter);
		echo $get;
	}

	public function get_data_ongkir()
	{
		$param = post('param');
		$get = $this->ongkir->get_kurir_data($param);
		echo $get;
	}
}
