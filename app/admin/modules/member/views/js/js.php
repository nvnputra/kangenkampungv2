<script type="text/javascript">
	function ajax_action_edit_active_member(id, status) {
		var form_data = new FormData();
		form_data.append('id', id);
		form_data.append('status', status);
		form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
		addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>member/ajax_action_active_member/", "POST", form_data);
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var form_data = new FormData();
		form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
		dataTableShow("#list", "<?php echo base_url() . $this->config->item('index_page'); ?>member/ajax_action_datatable_member", form_data);
	});
</script>