<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_admin');
		$this->load->library('recaptcha');
	}

	public function index()
	{
		$data['content'] = 'admin';
		$this->load->view('template', $data);
	}


	public function ajax_action_add_admin()
	{
		$this->form_validation->set_rules('name_admin', 'name_admin', 'required');
		$this->form_validation->set_rules('password_admin', 'password_admin', 'required');
		$this->form_validation->set_rules('ktp_admin', 'ktp_admin', 'required');
		$this->form_validation->set_rules('email_admin', 'email_admin', 'required|valid_email');


		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$data = array(
				"name_admin" => post("name_admin"),
				"password_admin" => md5(post("password_admin")),
				"ktp_admin" => post("ktp_admin"),
				"email_admin" => post("email_admin"),
				"status_admin" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_admin->insert_table("a__admin", $data);
			if ($add == FALSE) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . '/admin'
				);
				print json_encode($json_data);
			}
		}
	}

	function ajax_action_datatable_admin()
	{
		$column = "*";
		$table = "a__admin";
		$column_order = array('id_admin', 'name_admin', 'password_admin', 'ktp_admin', 'email_admin', 'created_at');
		$column_search = array('id_admin', 'name_admin', 'password_admin', 'ktp_admin', 'email_admin', 'created_at');
		$order = array('id_admin' => 'DESC');
		$where = '';
		$joins = '';
		$list = $this->M_admin->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);

		$link = '' . base_url() . $this->config->item('index_page') . '/admin';
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $key) {
			$no++;
			if ($key->status_admin == 1) {
				$status = "<badge class='badge badge-success'>Aktif</badge>";
			} else {
				$status = "<badge class='badge badge-danger'>Disable</badge>";
			}
			$row = array();
			$row[] = $no;
			$row[] = $key->name_admin;
			$row[] = $key->ktp_admin;
			$row[] = $key->email_admin;
			$row[] = $status;
			$row[] = '<input type="button" class="btn btn-danger btn-sm" onclick="ajaxItemDelete(' . "'" . $link . "'" . ',' . "'ajax_action_delete_admin'" . ',' . $key->id_admin . ')"  value="Hapus">
					<input type="button" class="btn btn-info btn-sm"  onclick="ajax_show_edit_modal(' . $key->id_admin . ')" value="Edit">';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_admin->count_all($table, $where, $joins),
			"recordsFiltered" => $this->M_admin->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
			"data" => $data,
		);

		echo json_encode($output);
	}

	function ajax_action_get_admin()
	{
		$id = $this->uri->segment(3);
		$data = $this->M_admin->fetch_table('*', 'a__admin', 'id_admin = ' . $id, '', '', 0, 0, TRUE);

		if (count($data) == 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
				"form_error" => '',
				"redirect" => '',
				"data" => $data
			);
			print json_encode($json_data);
		}
	}

	public function ajax_action_delete_admin()
	{
		$id = $this->uri->segment(3);
		$delete = $this->M_admin->delete_table("a__admin", "id_admin", $id);
		if ($delete == FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menghapus Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses Menghapus data'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . '/admin'
			);
			print json_encode($json_data);
		}
	}

	function ajax_action_edit()
	{
		$this->form_validation->set_rules('name_admin', 'name_admin', 'required');
		$this->form_validation->set_rules('ktp_admin', 'ktp_admin', 'required');
		$this->form_validation->set_rules('email_admin', 'email_admin', 'required|valid_email');


		### FORM VALIDATION
		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		}
		$id = $this->uri->segment(3);

		### TRANSACTION START
		$this->db->trans_start();
		if (post('password_admin') != '') {
			$data = array(
				"name_admin" => post("name_admin"),
				"password_admin" => md5(post("password_admin")),
				"ktp_admin" => post("ktp_admin"),
				"email_admin" => post("email_admin"),
				"status_admin" => 1,
				'update_at' => date('Y-m-d H:m:s')
			);
		} else {
			$data = array(
				"name_admin" => post("name_admin"),
				"ktp_admin" => post("ktp_admin"),
				"email_admin" => post("email_admin"),
				"status_admin" => 1,
				'update_at' => date('Y-m-d H:m:s')
			);
		}


		$edit = $this->M_admin->update_table('a__admin', $data, 'id_admin', $id);

		$this->db->trans_complete();
		### TRANSACTION END

		if ($edit == FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal update Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses update Data'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . '/admin/'
			);
			print json_encode($json_data);
		}
	}
}
