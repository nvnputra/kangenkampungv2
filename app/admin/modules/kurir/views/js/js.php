<script type="text/javascript">
    $(document).ready(function() {
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          dataTableShow("#list_kurir","<?php echo base_url().$this->config->item('index_page'); ?>kurir/ajax_list_kurir",form_data);
    });

</script>

<script type="text/javascript">
  // Add kurir

    function ajax_action_add_kurir(){
      var file_data = $('#attachment_kurir').prop('files')[0];
          var form_data = new FormData();
          form_data.append('file', file_data);
          form_data.append('name_kurir', $('#name_kurir').val());
          form_data.append('api_kurir', $('#api_kurir').val());
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
      addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>/kurir/ajax_action_add_kurir/","POST",form_data);
        }

     function ajax_show_edit_modal(id){
          var link = "<?php echo base_url().$this->config->item('index_page'); ?>kurir/ajax_action_get_detail_pemesanan/"+id;
           var form_data = new FormData();
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
            
          ajaxShowData(link,"POST",form_data,function(response) {
              var name_kurir =$('#edit_name_kurir').val(response.data[0].name_kurir);
              var id_file =$('#edit_id_file_kurir').val(response.data[0].id_file);
              var id_kurir =$('#edit_id_kurir').val(response.data[0].id_kurir);
              var api_kurir =$('#edit_api_kurir').val(response.data[0].api_kurir);
              $("#edit_kurir_preview").attr("src","<?= base_url().'uploads/kurir/';?>"+response.data[0].nama_file);
              $("#status_kurir option[value='"+response.data[0].status_kurir+"']").attr('selected','selected');
             $('#page-load').hide();
          });
         $('#edit-kurir').modal("show");
       
        }

         function ajax_action_edit_kurir(){
           var file_data = $('#edit_attachment_kurir').prop('files')[0];
          var form_data = new FormData();
          var name_kurir =$('#edit_name_kurir').val();
          var id_kurir =$('#edit_id_kurir').val();
          var api_kurir =$('#edit_api_kurir').val();
          var id_file =$('#edit_id_file_kurir').val();
          var status_kurir = $('#status_kurir :selected').val();
          form_data.append('status_kurir',status_kurir);
          form_data.append('api_kurir', api_kurir);
          form_data.append('id_kurir', id_kurir);
          form_data.append('id_file', id_file);
          form_data.append('status_kurir', status_kurir);
          form_data.append('name_kurir', name_kurir);
           form_data.append('file', file_data);
          form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
          addItemSerialize("<?php echo base_url().$this->config->item('index_page'); ?>kurir/ajax_action_edit_kurir/","POST",form_data);
            }

</script>