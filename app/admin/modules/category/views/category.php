<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Kategori Barang</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-0">Tambah Kategori Barang</h3>
                </div>
                <div class="col-6" style="text-align: right">
                    <button class="btn btn-primary" type="submit" id="show-category"><i class="fas fa-chevron-down"></i></button>
                </div>
            </div>

        </div>
        <!-- Card body -->
        <div class="card-body" id="form-add-categori" style="display: none">
            <!-- Form groups used in grid -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-control-label" for="example3cols1Input">Nama Kategori</label>
                        <input type="text" class="form-control" id="name_category" placeholder="Masukkan Nama Kategori">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-control-label" for="example3cols2Input">Status Kategori</label>
                        <select class="form-control" id="status_category">
                            <option selected value="1">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <label class="form-control-label" for="example3cols1Input">Gambar Kategory</label>
                        <input type="file" name="" class="form-control" name="file" accept="image/*" id="attachment" onchange="loadFile(event,'image_preview_category')">
                    </div>
                    <div style="margin-top: 7%">
                        <center>
                            <img src="" id="image_preview_category">
                        </center>
                    </div>
                </div>
            </div>
            <div style="text-align: right">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" onclick="ajax_action_add_category()">Tambah Kategori</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <h3 class="mb-0">Data Kategori Barang</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit-category">Edit Kategori</button> -->
            <div class="table-responsive py-4">
                <table class="table" id="list_category" style="width: 100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name Category</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- modal edit -->

<!-- Modal -->
<div id="edit-category" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="card-wrapper">
                <!-- Input groups -->
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <h3 class="mb-0">Ganti Category</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols1Input">Nama Kategori</label>
                                    <input type="text" class="form-control" id="edit_name_category" placeholder="Masukkan Nama Kategori">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols2Input">Status Kategori</label>
                                    <select class="form-control" id="edit_status_category">
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label class="form-control-label" for="example3cols1Input">Nama Kategori</label>
                                    <input type="file" name="" onchange="loadFile(event,'edit_category_preview')" class="form-control" name="file" accept="image/*" id="edit_attachment">
                                </div>
                                <div style="margin-top: 5%">
                                    <center>
                                        <img src="" id="edit_category_preview">
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-6" style="display: none">
                                <div class="form-group">
                                    <label class="form-control-label" for="example3cols1Input">Gambar Kategory</label>
                                    <input type="text" class="form-control" id="edit_id_category" placeholder="Masukkan Nama Kategori">
                                    <input type="text" class="form-control" id="edit_id_file" placeholder="Masukkan Nama Kategori">
                                </div>
                            </div>
                        </div>
                        <div style="text-align: right">
                            <button class="btn btn-primary" type="submit" onclick="ajax_action_edit_category()">Ganti Kategori</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<?php include("js/js.php"); ?>