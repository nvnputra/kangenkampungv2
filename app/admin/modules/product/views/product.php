<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Data Produk</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <h3 class="mb-0">Data Produk</h3>
        </div>

        <!-- Card body -->
        <div class="card-body">
            <div style="text-align: right">
                <div class="form-group">
                    <a href="<?php echo base_url() . index_page(); ?>product/add_product" style="color: white"><button class="btn btn-primary" type="submit">Tambah Produk</button></a>
                </div>
            </div>
            <div class="table-responsive py-4">
                <table class="table" id="list_product">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Product</th>
                            <th scope="col">Nama Merchant</th>
                            <th scope="col">Nama Category</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Status</th>
                            <th scope="col">Preview</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>



<?php include("js/js.php") ?>