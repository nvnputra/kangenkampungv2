<script type="text/javascript">
  $(document).ready(function() {
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    dataTableShow("#list_product", "<?php echo base_url() . $this->config->item('index_page'); ?>product/ajax_action_datatable_product", form_data);
  });
</script>

<script type="text/javascript">
  function disabled_product(id) {
    var form_data = new FormData();
    form_data.append('id_product', id);
    form_data.append('status', 0);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/product/disabled_product", "POST", form_data);
  }

  function enable_product(id) {
    var form_data = new FormData();
    form_data.append('id_product', id);
    form_data.append('status', 1);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/product/disabled_product", "POST", form_data);

  }

  function delete_product(id) {
    var form_data = new FormData();
    form_data.append('id_product', id);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/product/hapus_product", "POST", form_data);

  }

  function preview_image(id) {
    var form_data = new FormData();
    form_data.append('id_product', id);
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
    addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>/product/hapus_product", "POST", form_data);

  }

  
</script>
