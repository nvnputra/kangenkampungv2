<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Tambah Produk</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .image-border-add-product {
        height: 270px;
        box-shadow: rgba(189, 189, 189, 0.4) 0px 5px 15px 0px;
        border-width: 1px;
        border-style: solid;

        border-color: rgb(94, 114, 228);
        border-image: initial;
    }
</style>

<div class="container-fluid mt--6">
    <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-0">Tambah Produk</h3>
                </div>
                <div class="col-6" style="text-align: right">
                    <button class="btn btn-primary" type="submit" id="show-product"><i class="fas fa-chevron-down"></i></button>
                </div>
            </div>

        </div>
        <!-- Card body -->
        <div class="card-body" id="form-add-product">
            <!-- Form groups used in grid -->
            <div class="card-body">
                <!-- Form groups used in grid -->
                <div>
                    <div class="">
                        <div class="card-tipis">
                            <div style="padding: 2%">
                                <div class="popins" style="font-weight: bold;font-size: 19px">Upload Product</div>
                                <div class="popins" style="font-weight: bold;font-size: 14px">Upload Foto product yang jelas , agar memudahkan customer anda , 4 foto harus terisi semua</div>
                            </div>
                            <div class="container">
                                <div style="padding: 2%">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input type="file" id="target_1" style="display: none" onchange="readURL(this,'#prev_1','#icon_1');" name="file" accept="image/*" />
                                            <div class="image-border-add-product pointer" style="cursor: pointer;" onclick="target('#target_1')">
                                                <img src="" style='display: block;
                                                      margin-left: auto;
                                                      margin-right: auto;
                                                      height: 240px;
                                                      position: relative;' id="prev_1">
                                                <div style="text-align: center;" id="icon_1">
                                                    <i class="nav-icon i-Upload1" style="font-size: 122px"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="file" id="target_2" style="display: none" onchange="readURL(this,'#prev_2','#icon_2');" accept="image/*" />
                                            <div class="image-border-add-product" style="cursor: pointer;" onclick="target('#target_2')"> <img src="" style='display: block;
                                                      margin-left: auto;
                                                      margin-right: auto;
                                                      height: 240px;
                                                      position: relative;' id="prev_2">
                                                <div style="text-align: center;" id="icon_2">
                                                    <i class="nav-icon i-Upload1" style="font-size: 122px"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="file" id="target_3" style="display: none" onchange="readURL(this,'#prev_3','#icon_3');" accept="image/*" />
                                            <div class="image-border-add-product" style="cursor: pointer;" onclick="target('#target_3')">
                                                <img src="" style='display: block;
                                                      margin-left: auto;
                                                      margin-right: auto;
                                                      height: 240px;
                                                      position: relative;' id="prev_3">
                                                <div style="text-align: center;" id="icon_3">
                                                    <i class="nav-icon i-Upload1" style="font-size: 122px"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="file" id="target_4" style="display: none" onchange="readURL(this,'#prev_4','#icon_4');" accept="image/*" />
                                            <div class="image-border-add-product" style="cursor: pointer;" onclick="target('#target_4')">
                                                <img src="" style='display: block;
                                                      margin-left: auto;
                                                      margin-right: auto;
                                                      height: 240px;
                                                      position: relative;' id="prev_4">
                                                <div style="text-align: center;" id="icon_4">
                                                    <i class="nav-icon i-Upload1" style="font-size: 122px"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="card-tipis">
                            <div style="padding: 2%">
                                <div style="font-weight: bold;font-size: 19px" class="popins">Informasi Product</div>
                            </div>
                            <div class="row" style="padding-left: 2%;padding-right: 2%">

                                <div class="col-sm-12">
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Nama Toko</div>
                                            <div class="popins" style="font-size: 9px">Pilih nama toko (merchant) yang ingin anda tambahkan product</div>

                                        </div>
                                        <!-- content -->
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <select class="form-control" id="id_merchant">
                                                <option>Pilih Toko (Merchant) </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Nama Produk</div>
                                            <div class="popins" style="font-size: 9px">Tulis nama produk sesuai jenis, merek, dan rincian produk</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <input type="text" name="" class="form-control" id="name_product">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Kategori Produk</div>
                                            <div class="popins" style="font-size: 9px">Pilih kategori product yang telah di sediakan</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <select class="form-control" id="kategory_product">
                                                <option>Pilih Kategori Produk </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Informasi Produk</div>
                                            <div class="popins" style="font-size: 9px">Tulis Informasi Produk di sini untuk membantu customer anda</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <textarea class="form-control" id="informasi_product">

                                </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                        </div>

                        <br>
                        <div class="card-tipis">
                            <div style="padding: 2%">
                                <div style="font-weight: bold;font-size: 19px" class="popins">Harga Product</div>
                            </div>
                            <div class="row" style="padding-left: 2%;padding-right: 2%">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Harga</div>
                                            <div class="popins" style="font-size: 9px">Tulis harga product</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Rp</span>
                                                </div>
                                                <input type="number" name="" class="form-control" id="harga_product">
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Minimum Pesanan</div>
                                            <div class="popins" style="font-size: 9px">Masukkan Minimm Pesanana Pada Product ini</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <input type="number" name="" class="form-control" id="minimum_product">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Stok</div>
                                            <div class="popins" style="font-size: 9px">Masukkan Stk Barang Pada Product ini</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <input type="number" name="" class="form-control" id="stok_product">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Diskon</div>
                                            <div class="popins" style="font-size: 9px">Berikan Diskon pada produk anda (%) </div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <div class="input-group mb-3">
                                                <input type="number" name="" value="0" class="form-control" id="diskon_product">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><b>(%)</b></span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                        </div>

                        <br>
                        <div class="card-tipis">
                            <div style="padding: 2%">
                                <div style="font-weight: bold;font-size: 19px" class="popins">Detail Product</div>
                            </div>
                            <div class="row" style="padding-left: 2%;padding-right: 2%">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Berat</div>
                                            <div class="popins" style="font-size: 9px">Berat Product dalam skala <b>GRAM</b></div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <input type="number" name="" class="form-control" id="berat_product">
                                        </div>
                                    </div>
                                </div>
                                <!-- <br> -->
                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Dimensi</div>
                                            <div class="popins" style="font-size: 9px">Dimensi Product dalam kemasan </div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <input type="number" name="" class="form-control" id="dimensi_product">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Expired</div>
                                            <div class="popins" style="font-size: 9px">Tanggal Expired Product anda</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <input type="date" name="" class="form-control" id="expired_product">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Lokasi / Khas</div>
                                            <div class="popins" style="font-size: 9px">Berikan Lokasi atau asal produk Anda </div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <select id="wilayah" class="form-control select2">

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <div class="row">
                                        <!-- title -->
                                        <div class="col-sm-2">
                                            <div class="popins" style="font-size: 15px">Catatan Toko</div>
                                            <div class="popins" style="font-size: 9px">Masukkan catatan pada product ini</div>

                                        </div>
                                        <!-- content -->
                                        <div class="col-sm-10" style="margin-top: 1%">
                                            <textarea class="form-control" id="catatan_product">

                                </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                        </div>



                    </div>
                </div>
            </div>
            <div style="text-align: right">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" onclick="tambah_product()">Tambah Produk</button>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    CKEDITOR.replace('catatan_product');
    CKEDITOR.replace('informasi_product');
</script>
<?php include("js/js_product.php") ?>