<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends MY_Controller
{

    public function __construct()
    {
        $this->load->model('M_product');
    }

    public function index()
    {
        $data['content'] = 'product';
        $data['page_active'] = 'product';
        $this->load->view('template', $data);
    }

    public function add_product()
    {
        $data['content'] = 'add-product/add_product';
        $data['page_active'] = 'product';
        $this->load->view('template', $data);
    }

    public function edit_product()
    {
        $data['content'] = 'edit-product/edit_product';
        $data['page_active'] = 'product';
        $this->load->view('template', $data);
    }

    public function trash_product()
    {
        $data['content'] = 'trash-product/trash_product';
        $data['page_active'] = 'product';
        $this->load->view('template', $data);
    }

    function ajax_action_datatable_product()
    {
        $column = "*";
        $table = "mr__product a";
        $column_order = array('name_product', 'name_category');
        $column_search = array('name_product', 'name_category');
        $order = array('id_product' => 'DESC');
        $where = "";
        $joins = array(
            array(
                'table' => 'a__category b',
                'condition' => 'a.id_kategory = b.id_category',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant c',
                'condition' => 'c.id_merchant = a.id_merchant',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail d',
                'condition' => 'a.id_product = d.id_product',
                'jointype' => ''
            )
        );
        $list = $this->M_product->get_datatables($column, $table, $column_order, $column_search, $order, $where, $joins);

        $link = '' . base_url() . $this->config->item('index_page') . 'product';
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            if ($key->status_product == 1) {
                $status = "<badge class='badge badge-success'>Aktif</badge>";
                $btn_status = '<input type="button" class="btn btn-danger btn-sm" onclick="disabled_product(\'' . $key->id_product . '\')"  value="Hapus">';
            } else {
                $status = "<badge class='badge badge-danger'>Disable</badge>";
                $btn_status = '<input type="button" class="btn btn-success btn-sm" onclick="enable_product(\'' . $key->id_product . '\')"  value="Aktifkan">';
            }
            $row[] = $no;
            $row[] = $key->name_product;
            $row[] = $key->name_merchant;
            $row[] = $key->name_category;
            $row[] = $key->stok_product;
            $row[] = $status;
            $row[] = "<img onclick=" . "preview(\"" . base_url() . "uploads/product/" . $this->ajax_action_get_image_preview($key->id_product)  . "\")" . " src=" . base_url() . "uploads/product/" . $this->ajax_action_get_image_preview($key->id_product) . " style='height: 10%;cursor:pointer'> ";
            $row[] =   $btn_status .
                '<a href="' . $link . "/edit_product/" . $key->id_product . '"><input type="button" class="btn btn-info btn-sm"  value="Edit"></a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_product->count_all($table, $where, $joins),
            "recordsFiltered" => $this->M_product->count_filtered($column, $table, $column_order, $column_search, $order, $where, $joins),
            "data" => $data,
        );

        echo json_encode($output);
    }



    public function ajax_action_edit_product()
    {
        $id = $this->input->post('id_product');
        $this->form_validation->set_rules('id_merchant', 'id_merchant', 'required');
        $this->form_validation->set_rules('id_kategory', 'id_kategory', 'required');
        $this->form_validation->set_rules('minimum_product', 'minimum_product', 'required');
        $this->form_validation->set_rules('name_product', 'name_product', 'required');
        $this->form_validation->set_rules('catatan_product', 'catatan_product', 'required');
        $this->form_validation->set_rules('berat_product', 'berat_product', 'required');
        $this->form_validation->set_rules('harga_product', 'harga_product', 'required');
        $this->form_validation->set_rules('stok_product', 'stok_product', 'required');
        $this->form_validation->set_rules('lokasi_product', 'lokasi_product', 'required');
        $this->form_validation->set_rules('id_product', 'id_product', 'required');
        if ($this->form_validation->run() == FALSE) {
            $error     = $this->form_validation->error_array();
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Pastikan Data terisi Semua'
                ),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {

            $data = array(
                "id_kategory" => post("id_kategory"),
                "id_merchant" => post("id_merchant"),
                "minimum_product" => post("minimum_product"),
                "name_product" => post("name_product"),
                "catatan_product" => post("catatan_product"),
                "berat_product" => post("berat_product"),
                "dimensi_product" => post("dimensi_product"),
                "update_at" => date("Y-m-d H:i:s")
            );
            $data_detail = array(
                "stok_product" => post("stok_product"),
                "lokasi_product" => post("lokasi_product"),
                "diskon_product" => post("diskon_product"),
                "kota_product" => post("kota_product"),
                "informasi_product" => post("informasi_product"),
                "harga_product" => post("harga_product"),
                "expired_product" => post("expired_product"),
                "update_at" => date("Y-m-d H:i:s")
            );

            $update = $this->M_product->update_table('mr__product', $data, 'id_product', $id);
            $update_detail = $this->M_product->update_table('mr__product_detail', $data_detail, 'id_product', $id);
            if ($update_detail == FALSE) {
                $json_data = array(
                    "result" => FALSE,
                    "message" => array(
                        'head' => 'Failed',
                        'body' => 'Gagal Menambah Data'
                    ),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            } else {

                $this->update_upload_file('file', './uploads/product/', $id, 'mr__product', '1');
                $this->update_upload_file('file2', './uploads/product/', $id, 'mr__product', '2');
                $this->update_upload_file('file3', './uploads/product/', $id, 'mr__product', '3');
                $this->update_upload_file('file4', './uploads/product/', $id, 'mr__product', '4');
                // $this->update_upload_file('file5', './uploads/product/', $id, 'mr__product','5');
                // $this->update_upload_file('file6', './uploads/product/', $id, 'mr__product','6');
                $json_data = array(
                    "result" => TRUE,
                    "message" => array(
                        'head' => 'Success',
                        'body' => 'Sukses Merubah data'
                    ),
                    "form_error" => '',
                    "redirect" => '' . base_url() . $this->config->item('index_page') . '/product'
                );
                print json_encode($json_data);
            }
        }
    }

    public function ajax_action_add_product()
    {
        $this->form_validation->set_rules('id_merchant', 'id_merchant', 'required');
        $this->form_validation->set_rules('id_kategory', 'id_kategory', 'required');
        $this->form_validation->set_rules('minimum_product', 'minimum_product', 'required');
        $this->form_validation->set_rules('name_product', 'name_product', 'required');
        $this->form_validation->set_rules('catatan_product', 'catatan_product', 'required');
        $this->form_validation->set_rules('berat_product', 'berat_product', 'required');
        $this->form_validation->set_rules('harga_product', 'harga_product', 'required');
        $this->form_validation->set_rules('stok_product', 'stok_product', 'required');
        $this->form_validation->set_rules('lokasi_product', 'lokasi_product', 'required');
        if ($this->form_validation->run() == FALSE) {
            $error     = $this->form_validation->error_array();
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Pastikan Data terisi Semua'
                ),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $this->db->trans_start();
            $count_data_product   = $this->M_product->get_count_data('mr__product');
            $count_data_product_detail   = $this->M_product->get_count_data('mr__product_detail');
            $id_merchant_product = "PRD" . date('his') . $count_data_product;
            $id_product_detail = "PRDT" . date('his') . $count_data_product_detail;
            $this->upload_file('file', './uploads/product/', $id_merchant_product, 'mr__product', '1');
            $this->upload_file('file2', './uploads/product/', $id_merchant_product, 'mr__product', '2');
            $this->upload_file('file3', './uploads/product/', $id_merchant_product, 'mr__product', '3');
            $upload = $this->upload_file('file4', './uploads/product/', $id_merchant_product, 'mr__product', '4');
            // $this->upload_file('file5', './uploads/product/', $id_merchant_product, 'mr__product','5');
            // $upload = $this->upload_file('file6', './uploads/product/', $id_merchant_product, 'mr__product','6');

            if ($upload) {
                $data = array(
                    "id_product" => $id_merchant_product,
                    "id_merchant" => post("id_merchant"),
                    "id_kategory" => post("id_kategory"),
                    "status_product" => 1,
                    "minimum_product" => post("minimum_product"),
                    "name_product" => post("name_product"),
                    "catatan_product" => post("catatan_product"),
                    "berat_product" => post("berat_product"),
                    "dimensi_product" => post("dimensi_product"),
                    "created_at" => date("Y-m-d H:i:s")
                );
                $data_detail = array(
                    "id_product_detail" => $id_product_detail,
                    "id_product" => $id_merchant_product,
                    "kota_product" => post("kota_product"),
                    "stok_product" => post("stok_product"),
                    "lokasi_product" => post("lokasi_product"),
                    "diskon_product" => post("diskon_product"),
                    "informasi_product" => post("informasi_product"),
                    "harga_product" => post("harga_product"),
                    "expired_product" => post("expired_product"),
                    "created_at" => date("Y-m-d H:i:s")
                );
                $add  = $this->M_product->insert_table("mr__product", $data);
                $add_detail  = $this->M_product->insert_table("mr__product_detail", $data_detail);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $json_data = array(
                        "result" => FALSE,
                        "message" => array(
                            'head' => 'Failed',
                            'body' => 'Gagal Menambah Data'
                        ),
                        "form_error" => $error,
                        "redirect" => ''
                    );
                    print json_encode($json_data);
                    $this->db->trans_commit();
                    die();
                    return TRUE;
                } else {
                    $json_data = array(
                        "result" => TRUE,
                        "message" => array(
                            'head' => 'Success',
                            'body' => 'Sukses Mengisi data'
                        ),
                        "form_error" => '',
                        "redirect" => '' . base_url() . $this->config->item('index_page') . 'product'
                    );
                    print json_encode($json_data);
                    $this->db->trans_rollback();
                    return FALSE;
                }
            }
        }
    }
    public function upload_file($file, $location, $id, $relation, $position_file)
    {
        if (isset($_FILES[$file]) && is_uploaded_file($_FILES[$file]['tmp_name'])) {
            /*-------------setting attachment upload -------------*/
            $config['upload_path']   = $location;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = 1024 * 8;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file)) {
                $data['file_name'] = null;
                $json_data         = array(
                    "result" => FALSE,
                    "message" => array(
                        'head' => 'Failed',
                        'body' => $this->upload->display_errors('', '')
                    ),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            } else {
                $data = $this->upload->data();
                return $this->add_file_db($data['file_name'], $relation, $id, $position_file);
            }
        } else {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => "Harus Mengupload Gambar "
                ),
                "form_error" => 'file proof',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }
    }

    public function update_upload_file($file, $location, $id, $relation, $position_file)
    {
        if (isset($_FILES[$file]) && is_uploaded_file($_FILES[$file]['tmp_name'])) {
            /*-------------setting attachment upload -------------*/
            $config['upload_path']   = $location;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = 1024 * 8;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file)) {
                $data['file_name'] = null;
                $json_data         = array(
                    "result" => FALSE,
                    "message" => array(
                        'head' => 'Failed',
                        'body' => $this->upload->display_errors('', '')
                    ),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            } else {
                $data = $this->upload->data();
                return $this->update_file_db($data['file_name'], $relation, $id, $position_file);
            }
        } else {
        }
    }


    public function add_file_db($name, $relation, $id_relation, $position_file)
    {
        $data = array(
            "name_file" => $name,
            "id_relation" => $id_relation,
            "table_relation" => $relation,
            "position_file" => $position_file,
            "created_at" => date("Y-m-d H:i:s")
        );
        $add  = $this->M_product->insert_table("mr__file", $data);
        if ($add) {
            return true;
        } else {
            return false;
        }
    }


    public function update_file_db($name, $relation, $id_relation, $position_file)
    {
        $data['file_name_unlink'] = $this->M_product->get_row("name_file", "mr__file", "id_relation='" . $id_relation . "'" . " AND " . "position_file='" . $position_file . "'", "", "", FALSE)->name_file;
        // echo $data['file_name_unlink'];
        // die();
        unlink('./uploads/product/' .  $data['file_name_unlink']);
        $data = array(
            "name_file" => $name,
            "update_at" => date("Y-m-d H:i:s")
        );
        $edit  = $this->M_product->update_table_2_param("mr__file", $data, "id_relation='" . $id_relation . "'" . " AND " . "position_file='" . $position_file . "'");
        if ($edit) {
            return true;
        } else {
            return false;
        }
    }

    public function get_data_product_by_id()
    {
        $id = $this->input->post('id_product');
        $joins = array(
            array(
                'table' => 'a__category b',
                'condition' => 'a.id_kategory = b.id_category',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant c',
                'condition' => 'c.id_merchant = a.id_merchant',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail d',
                'condition' => 'a.id_product = d.id_product',
                'jointype' => ''
            )
        );
        $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_product = ' . "'" . $id . "'", '', TRUE);
        $data['image'] = $this->M_product->fetch_table('name_file ', 'mr__file', 'id_relation = ' . "'" . $id . "'", '', '', '', '', TRUE);
        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    public function get_data_product_by_all()
    {
        $id = $this->session->userdata('id');
        $joins = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
        );
        $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_merchant = ' . "'" . $id . "'" . " AND " . 'a.status_product != 3', TRUE);
        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }


    public function get_data_product_by_all_trash()
    {
        $id = $this->session->userdata('id');
        $joins = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
        );
        $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_merchant = ' . "'" . $id . "'" . " AND " . 'a.status_product = 3', TRUE);
        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    public function disabled_product()
    {
        $id = $this->input->post('id_product');
        $data = array(
            "status_product" => $this->input->post('status')
        );
        $edit = $this->M_product->update_table('mr__product', $data, 'id_product', $id);
        $this->db->trans_complete();
        if ($edit == FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal Merubah Data'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Data etalase di update'),
                "form_error" => '',
                "redirect" => '' . base_url() . $this->config->item('index_page') . 'product'
            );
            print json_encode($json_data);
        }
    }

    public function hapus_product()
    {
        $id = $this->input->post('id_product');
        $data = array(
            "status_product" => 3
        );
        $edit = $this->M_product->update_table('mr__product', $data, 'id_product', $id);
        $this->db->trans_complete();
        if ($edit == FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal Merubah Data'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Data di Hapus dari etalase toko'),
                "form_error" => '',
                "redirect" => '' . base_url() . $this->config->item('index_page') . 'product'
            );
            print json_encode($json_data);
        }
    }

    function ajax_action_get_image_preview($id)
    {
        $data = $this->M_product->fetch_table('name_file', 'mr__file', 'id_relation = ' . "\"" . $id . "\"", '', '', 1, TRUE);
        return $data[0]->name_file;
    }
}
