<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/*-------- BAHAN DROPDOWN --------*/
if(!function_exists('ticket_number')) {
	function ticket_number($param = NULL) {
		$CI = &get_instance();
		$CI->load->database();

		$data = array();
		$CI->db->select('value');
		$CI->db->from('sys__config');
		$CI->db->where('param', 'ticket-prefix');
		$data1 = $CI->db->get();
		$prefix = $data1->row()->value;

		$CI->db->select('value');
		$CI->db->from('sys__config');
		$CI->db->where('param', 'ticket-number');
		$data2 = $CI->db->get();
		$number = $data2->row()->value;
		$update_id = sprintf('%07s', $number + 1);
		if ($param == 'prefix') {
			$data = $prefix;
		}else if ($param == 'number') {
			$data = $update_id;
		}else{
			$data = $prefix. $update_id;
		}
		return $data;
	}
}