<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends MY_Controller
{

    public function __construct()
    {
        $this->load->model('M_product');
    }

    public function home($param, $param2)
    {
        $data['content']  = 'product_category';
        $this->load->view('template', $data);
    }

    public function get_data_product_by_id()
    {
        $id              = $this->input->post('id_product');
        $joins           = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'a__category c',
                'condition' => 'a.id_kategory = c.id_category',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant d',
                'condition' => 'a.id_merchant = d.id_merchant',
                'jointype' => ''
            )
        );
        $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_product = ' . "'" . $id . "'", '', TRUE);
        $data['image']   = $this->M_product->fetch_table('name_file ', 'mr__file', 'id_relation = ' . "'" . $id . "'", '', '', '', '', TRUE);
        if (count($data) == 0) {
            $json_data = array(
                "result" => FALSE,
                "message" => array(
                    'head' => 'Failed',
                    'body' => 'Gagal mengambil Data'
                ),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data = array(
                "result" => TRUE,
                "message" => array(
                    'head' => 'Success',
                    'body' => 'Sukses mengambil Data'
                ),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    public function get_data_product_by_all()
    {
        $joins = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant d',
                'condition' => 'd.id_merchant = a.id_merchant ',
                'jointype' => ''
            )
        );
        $data = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.status_product != 0', TRUE);
        // $json = json_encode($data);
        // $objects = json_decode($json);
        // $grouped = array();
        // $i = 1;
        // foreach ($objects as $object) {
        //     if (!array_key_exists($object->id_merchant, $grouped)) {
        //         $newObject = new stdClass();
        //         $newObject->name_merchant = $object->name_merchant;
        //         $newObject->id_merchant = $object->id_merchant;
        //         $newObject->items = array();
        //         $grouped[$object->id_merchant] = $newObject;
        //     }
        //     $taskObject = new stdClass();

        //     if (count($newObject->items) == 0) {
        //         $taskObject->urut = count($newObject->items);
        //         $taskObject->name_product = $object->name_product;
        //         $taskObject->id_product = $object->id_product;
        //         $taskObject->harga_product = $object->harga_product;
        //         $taskObject->name_file = $object->name_file;
        //     } else {
        //         $taskObject->urut = count($newObject->items);
        //         $taskObject->id_product = $object->id_product;
        //         $taskObject->name_product = $object->name_product;
        //         $taskObject->harga_product = $object->harga_product;
        //         $taskObject->name_file = $object->name_file;
        //     }

        //     $grouped[$object->id_merchant]->items[] = $taskObject;
        //     $i++;
        // }

        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    public function get_data_product_by_all_category()
    {
        $category = urldecode(post('category'));
        $search = urldecode(post('search'));
        $joins = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant d',
                'condition' => 'd.id_merchant = a.id_merchant ',
                'jointype' => ''
            ),
            array(
                'table' => 'a__category e',
                'condition' => 'a.id_kategory = e.id_category',
                'jointype' => ''
            )
        );
        if ($category == "0") {
            $data = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.status_product != 3  AND a.name_product LIKE ' . '\'%' . $search . '%\'', TRUE);
        } else {
            if ($search == "a") {
                $data = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.status_product != 3 AND e.name_category LIKE ' . '\'%' . $category . '%\'', TRUE);
            } else {
                $data = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.status_product != 3 AND e.name_category LIKE ' . '\'%' . $category . '%\' AND a.name_product LIKE ' . '\'%' . $search . '%\'', TRUE);
            }
        }
        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    function cleanURL($textURL)
    {
        $URL = strtolower(preg_replace(array('/[^a-z0-9\- ]/i', '/[ \-]+/'), array('', '-'), $textURL));
        return $URL;
    }






    public function get_data_product_by_all_trash()
    {
        $id = $this->session->userdata('id');
        $joins = array(
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
        );
        $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_merchant = ' . "'" . $id . "'" . " AND " . 'a.status_product = 3', TRUE);
        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    public function disabled_product()
    {
        $id = $this->input->post('id_product');
        $data = array(
            "status_product" => $this->input->post('status')
        );
        $edit = $this->M_product->update_table('mr__product', $data, 'id_product', $id);
        $this->db->trans_complete();
        if ($edit == FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal Merubah Data'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Data etalase di update'),
                "form_error" => '',
                "redirect" => '' . base_url() . $this->config->item('index_page') . 'product'
            );
            print json_encode($json_data);
        }
    }

    public function hapus_product()
    {
        $id = $this->input->post('id_product');
        $data = array(
            "status_product" => 3
        );
        $edit = $this->M_product->update_table('mr__product', $data, 'id_product', $id);
        $this->db->trans_complete();
        if ($edit == FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal Merubah Data'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Data di Hapus dari etalase toko'),
                "form_error" => '',
                "redirect" => '' . base_url() . $this->config->item('index_page') . 'product'
            );
            print json_encode($json_data);
        }
    }

    function ajax_action_get_image_preview($id)
    {
        $data = $this->M_product->fetch_table('name_file', 'mr__file', 'id_relation = ' . "\"" . $id . "\"", '', '', 1, TRUE);
        return $data[0]->name_file;
    }
}
