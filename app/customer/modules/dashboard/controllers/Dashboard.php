<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_dashboard');
	}

	public function index()
	{
		$data['merchant'] = $this->M_dashboard->fetch_joins('mr__merchant a', 'id_merchant,name_merchant', '', '', '', TRUE);
		$data['content'] = 'dashboard';
		$this->load->view('template', $data);
	}
}
