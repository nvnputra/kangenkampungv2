<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <?php
        if (!$this->agent->is_mobile()) {
            $imgHeight = 'style="height:270px;"';
            $paddingButton = '';
            $paddingButton2 = '';
        } else {
            $imgHeight = 'style="height:150px;"';
            $paddingButton = 'style="padding:10px!important;font-size:12px!important;"';
            $paddingButton2 = 'padding:10px!important;font-size:12px!important;';
        }
        ?>
        <div class="row">

            <!-- section title -->
            <!-- section title -->
            <div class="col-md-12">
                <div class="section-title">
                    <h3 class="title">Produk </h3>
                </div>
            </div>

            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <!-- <div class="col-md-12">
                            <div class="section-title text-center">
                                <h3 class="title">Lihat Juga Produk ini</h3>
                            </div>
                        </div> -->
                        <div id="div_product">
                        </div>
                        <div class="clearfix visible-sm visible-xs"></div>




                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
        </div>

        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!-- /NEWSLETTER -->
<script type="text/javascript">
    // A $( document ).ready() block.
    $(document).ready(function() {
        get_data_product_by_all();
    });

    function get_data_product_by_all() {
        var i, j, x = "";
        var link = "<?php echo base_url() . index_page(); ?>product/get_data_product_by_all/";
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        var html = "";
        var top = "";
        var b = 0;
        var c = 0;
        ajaxShowData(link, "POST", form_data, function(response) {
            <?php if (!$this->agent->is_mobile()) { ?>
                for (i = 0; i < response.data.length; i++) {
                    b = i + 1;
                    if (b % 4 == 0) {
                        html += "<div class='row'>";
                    }
                    html += '<div class="col-xs-6 col-sm-3">' +
                        '<div class="product">' +
                        '<div class="product-img">' +
                        '<img <?= $imgHeight ?> src="<?php echo base_url() . "uploads/product/"; ?>' + response.data[i].name_file + '" alt="">' +
                        '<div class="product-label">' +
                        // '<span class="sale">' + i + '</span>' +
                        // '<span class="new">NEW</span>' +
                        '</div>' +
                        '</div>' +
                        '<div class="product-body">' +
                        '<p class="product-category">' + response.data[i].name_merchant + '</p>' +
                        '<h3 class="product-name">' + response.data[i].name_product + '</h3>' +
                        '<h4 class="product-price">' + addNumber(response.data[i].harga_product, true) +
                        // '<del class="product-old-price">' + response.data[i].harga_product + '</del>' +
                        '</h4>' +
                        '</div>' +
                        '<div style="text-align:center">' +
                        '<a href="<?php echo base_url() . index_page() . 'products/'; ?>' + response.data[i].id_product + '"> <button class="primary-btn order-submit" <?= $paddingButton ?>><i class="fa fa-shopping-cart"></i> Lihat Produk</button></a>' +
                        `<button class="primary-btn order-submit" style="margin-top: 5px;<?= $paddingButton2 ?>" onClick="chat('Saya Ingin bertanya tentang produk ${response.data[i].name_product} ini ?')"><i class="fa fa-whatsapp"></i> Tanya ` +
                        'Admin</button>' +
                        '</div>' +
                        '<br></div>' +
                        '</div>' +
                        '</div>';
                    if (b % 4 == 0) {
                        html += "</div>";
                    }

                }
            <?php } else { ?>
                for (i = 0; i < response.data.length; i++) {
                    c = i + 1;
                    if (c % 2 == 0) {
                        html += "<div class='row'>";
                    }
                    html += '<div class="col-xs-6 col-sm-3">' +
                        '<div class="product">' +
                        '<div class="product-img">' +
                        '<img <?= $imgHeight ?> src="<?php echo base_url() . "uploads/product/"; ?>' + response.data[i].name_file + '" alt="">' +
                        '<div class="product-label">' +
                        // '<span class="sale">' + i + '</span>' +
                        // '<span class="new">NEW</span>' +
                        '</div>' +
                        '</div>' +
                        '<div class="product-body">' +
                        '<p class="product-category">' + response.data[i].name_merchant + '</p>' +
                        '<h3 class="product-name">' + response.data[i].name_product + '</h3>' +
                        '<h4 class="product-price">' + addNumber(response.data[i].harga_product, true) +
                        // '<del class="product-old-price">' + response.data[i].harga_product + '</del>' +
                        '</h4>' +
                        '</div>' +
                        '<div style="text-align:center">' +
                        '<a href="<?php echo base_url() . index_page() . 'products/'; ?>' + response.data[i].id_product + '"> <button class="primary-btn order-submit" <?= $paddingButton ?>><i class="fa fa-shopping-cart"></i> Lihat Produk</button></a>' +
                        `<button class="primary-btn order-submit" style="margin-top: 5px;<?= $paddingButton2 ?>" onClick="chat('Saya Ingin bertanya tentang produk ${response.data[i].name_product} ini ?')"><i class="fa fa-whatsapp"></i> Tanya ` +
                        'Admin</button>' +
                        '</div>' +
                        '<br></div>' +
                        '</div>' +
                        '</div>';
                    if (b % 2 == 0) {
                        html += "</div>";
                    }

                }
            <?php } ?>


            $('#div_product').html(html);

        });

    }
</script>