<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends MY_Controller
{

    public $rand_num = 0;

    public function __construct()
    {
        $this->load->model('M_category');
    }
    function ajax_action_get_all_category()
    {
        $joins = array(
            array(
                'table' => 'a__file b',
                'condition' => 'a.id_file = b.id_file',
                'jointype' => ''
            ),
        );
        $data = $this->M_category->fetch_joins('a__category a', '*,b.nama_file ',  $joins, 'a.status_category = 1', '', TRUE);

        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }
}
