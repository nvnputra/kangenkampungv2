<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <?php
        if (!$this->agent->is_mobile()) {
            $imgHeight = 'style="height:270px;"';
        } else {
            $imgHeight = 'style="height:150px;"';
        }
        ?>
        <div class="row">

            <!-- section title -->
            <!-- section title -->

            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-4">
                            <div class="section-title text-center">
                                <h3 class="title">Detail Produk</h3>
                            </div>
                            <div style="text-align: center">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2">Total</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h1 id="total"></h1>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <a href="#" class="primary-btn order-submit" style="width:100%" onclick="set_alamat_keranjang()">Pesan</a>

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div id="div_product" class="row">
                            </div>
                        </div>

                        <div class="clearfix visible-sm visible-xs"></div>




                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
        </div>

        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /NEWSLETTER -->
<script type="text/javascript">
    // A $( document ).ready() block.
    $(document).ready(function() {
        keranjang_data();
    });

    function keranjang_data() {
        var i, j, x = "";
        var link = "<?php echo base_url() . index_page(); ?>keranjang/keranjang_data/";
        var form_data = new FormData();
        form_data.append('category', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        var html = "";
        var total = 0;
        ajaxShowData(link, "POST", form_data, function(response) {
            if (response.result == false) {
                html += '<center>keranjang saat ini kosong</center>';
            } else {
                for (i = 0; i < response.data.length; i++) {
                    total += (parseInt(response.data[i].harga_product) * parseInt(response.data[i].qty));
                    html += '<div class="col-xs-6 col-sm-4">' +
                        '<div class="product">' +
                        '<div class="product-img">' +
                        '<img <?= $imgHeight ?> src="<?php echo base_url() . "uploads/product/"; ?>' + response.data[i].name_file + '" alt="">' +
                        '<div class="product-label">' +
                        // '<span class="sale">-30%</span>' +
                        // '<span class="new">NEW</span>' +
                        '</div>' +
                        '</div>' +
                        '<div class="product-body">' +
                        '<p class="product-category">' + response.data[i].name_merchant + '</p>' +
                        '<h3 class="product-name">' + response.data[i].name_product + '</h3>' +
                        '<h4 class="product-price">' + addNumber(response.data[i].harga_product, true) +
                        '<br>' +
                        `<br><a href="#" class="btn btn-danger" style="width:100%" onclick="hapus('${response.data[i].id_cart}')">Hapus</a>` +
                        '</h4>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
            }
            $('#total').text(addNumber(total, true));
            $('#div_product').html(html);

        });

    }
</script>

<script>
    function hapus(id) {
        var form_data = new FormData();
        form_data.append('id_cart', id);
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>keranjang/hapus_keranjang/", "POST", form_data);
    }
</script>