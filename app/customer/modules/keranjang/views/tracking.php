<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <?php
        if (!$this->agent->is_mobile()) {
            $imgHeight = 'style="height:270px;"';
        } else {
            $imgHeight = 'style="height:150px;"';
        }
        ?>
        <div class="row">

            <!-- section title -->
            <!-- section title -->

            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-4">
                            <div class="section-title text-center">
                                <h3 class="title">Detail Produk</h3>
                            </div>
                            <div style="text-align: center">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2">Total</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h1 id="total"></h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status Pembelian
                                        </td>
                                        <td>
                                            <div id="status_pembelian"></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div id="div_product" class="row">
                            </div>
                        </div>

                        <div class="clearfix visible-sm visible-xs"></div>




                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
        </div>

        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /NEWSLETTER -->
<script type="text/javascript">
    // A $( document ).ready() block.
    $(document).ready(function() {
        keranjang_data();
    });

    function keranjang_data() {
        var i, j, x = "";
        var link = "<?php echo base_url() . index_page(); ?>keranjang/tracking/";
        var form_data = new FormData();
        form_data.append('id_order', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        var html = "";
        var total = 0;
        var text = "";
        var btn = "";
        ajaxShowData(link, "POST", form_data, function(response) {
            if (response.result == false) {
                html += '<center>keranjang saat ini kosong</center>';
            } else {
                for (i = 0; i < response.data.length; i++) {
                    total += (parseInt(response.data[i].harga_product) * parseInt(response.data[i].qty));
                    html += '<div class="col-xs-6 col-sm-4">' +
                        '<div class="product">' +
                        '<div class="product-img">' +
                        '<img <?= $imgHeight ?> src="<?php echo base_url() . "uploads/product/"; ?>' + response.data[i].name_file + '" alt="">' +
                        '<div class="product-label">' +
                        // '<span class="sale">-30%</span>' +
                        // '<span class="new">NEW</span>' +
                        '</div>' +
                        '</div>' +
                        '<div class="product-body">' +
                        '<p class="product-category">' + response.data[i].name_merchant + '</p>' +
                        '<h3 class="product-name">' + response.data[i].name_product + '</h3>' +
                        '<h4 class="product-price">' + addNumber(response.data[i].harga_product, true) +
                        '<br>' +
                        '</h4>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }
                if (response.order[0].status_order == 1) {
                    text = "Pesan belum Di Cek";
                    btn = "btn btn-warning";
                } else if (response.order[0].status_order == 2) {
                    text = "Pesan Telah Dikirim";
                    btn = "btn btn-info";
                } else if (response.order[0].status_order == 3) {
                    text = "Bukti Telah Di Upload";
                    btn = "btn btn-info";
                } else if (response.order[0].status_order == 4) {
                    text = "Pembelian Berhasil";
                    btn = "btn btn-info";
                } else if (response.order[0].status_order == 5) {
                    text = "Barang Dikirim";
                    btn = "btn btn-info";
                } else if (response.order[0].status_order == 6) {
                    text = "Barang Sampai";
                    btn = "btn btn-success";
                } else if (response.order[0].status_order == 7) {
                    text = "Pesanan Di Batalkan";
                    btn = "btn btn-danger";
                }
                var status = `<button class='${btn}'>${text}</button>`;
                $('#status_pembelian').html(status)
            }
            $('#total').text(addNumber(total, true));
            $('#div_product').html(html);

        });

    }
</script>

\