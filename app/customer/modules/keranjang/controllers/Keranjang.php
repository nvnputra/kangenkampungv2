<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keranjang extends MY_Controller
{

    public function __construct()
    {
        $this->load->model('M_keranjang');
    }

    public function index()
    {
        $data['content']  = 'keranjang';
        $this->load->view('template', $data);
    }

    public function home($param)
    {
        $data['content']  = 'tracking';
        $this->load->view('template', $data);
    }

    public function tambah_keranjang()
    {
        $this->form_validation->set_rules('stok', 'stok', 'required');
        $this->form_validation->set_rules('id_product', 'id_product', 'required');
        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $this->db->trans_start();
            $last_id_member = $this->session->userdata("id");
            $count_data_order = $this->M_keranjang->get_count_data('m__cart');
            $id_cart    = "CART" . date('his') . $count_data_order;
            $dataproduk = $this->M_keranjang->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
            $dataorder = array(
                "id_cart" => $id_cart,
                "id_member" => $last_id_member,
                "id_product" => post('id_product'),
                "qty" => post('stok'),
                "status_cart" => 0,
                "created_at" => date("Y-m-d H:i:s")
            );
            $dataorder = $this->M_keranjang->insert_table("m__cart", $dataorder);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $json_data =  array(
                    "result" => FALSE,
                    "message" => array('head' => 'Failed', 'body' => 'Gagal Menambah keranjang'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            } else {
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head' => 'Success', 'body' => 'Pesanan anda di masukkan keranjang , silahkan cek pesanan anda di menu keranjang'),
                    "form_error" => '',
                    "redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
                );
                print json_encode($json_data);
            }
        }
    }

    public function keranjang_data()
    {
        $id_member = $this->session->userdata("id");
        $joins = array(
            array(
                'table' => 'mr__product f',
                'condition' => 'a.id_product = f.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant d',
                'condition' => 'd.id_merchant = f.id_merchant ',
                'jointype' => ''
            ),
            array(
                'table' => 'a__category e',
                'condition' => 'f.id_kategory = e.id_category',
                'jointype' => ''
            )
        );
        $data = $this->M_keranjang->fetch_joins('m__cart a', '*', $joins, 'a.status_cart = 0 AND a.id_member=' . '\'' . $id_member . '\'', TRUE);

        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data
            );
            print json_encode($json_data);
        }
    }

    public function tracking()
    {
        $id_order = post("id_order");
        $joins = array(
            array(
                'table' => 'mr__product f',
                'condition' => 'a.id_product = f.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__product_detail b',
                'condition' => 'a.id_product = b.id_product',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__file c',
                'condition' => 'c.id_relation = b.id_product AND c.position_file = 1',
                'jointype' => ''
            ),
            array(
                'table' => 'mr__merchant d',
                'condition' => 'd.id_merchant = f.id_merchant ',
                'jointype' => ''
            ),
            array(
                'table' => 'a__category e',
                'condition' => 'f.id_kategory = e.id_category',
                'jointype' => ''
            )
        );
        $data = $this->M_keranjang->fetch_joins('m__detail_order a', '*', $joins, 'a.status_cart = 1 AND a.id_order=' . '\'' . $id_order . '\'', TRUE);
        $order = $this->M_keranjang->fetch_joins('m__order', '*', '', ' id_order=' . '\'' . $id_order . '\'', TRUE);

        if (count($data) == 0) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal mengambil Data'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses mengambil Data'),
                "form_error" => '',
                "redirect" => '',
                "data" => $data,
                "order" => $order
            );
            print json_encode($json_data);
        }
    }

    public function hapus_keranjang()
    {
        $id = post("id_cart");
        $delete = $this->M_keranjang->delete_table("m__cart", "id_cart", $id);
        if ($delete == FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head' => 'Failed', 'body' => 'Gagal Menghapus Data'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        } else {
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head' => 'Success', 'body' => 'Sukses Menghapus data'),
                "form_error" => '',
                "redirect" => '' . base_url() . $this->config->item('index_page') . 'keranjang'
            );
            print json_encode($json_data);
        }
    }
}
