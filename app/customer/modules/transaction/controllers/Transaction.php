<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_transaction');
	}

	public function beli_now()
	{
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('tel', 'tel', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('provinsi', 'provinsi', 'required');
		$this->form_validation->set_rules('kota', 'kota', 'required');
		$this->form_validation->set_rules('kecamatan', 'kecamatan', 'required');
		$this->form_validation->set_rules('kelurahan', 'kelurahan', 'required');
		$this->form_validation->set_rules('alamat_lengkap', 'alamat_lengkap', 'required');
		$this->form_validation->set_rules('kodepos', 'kodepos', 'required');
		$this->form_validation->set_rules('stok', 'stok', 'required');
		$this->form_validation->set_rules('id_product', 'id_product', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {

			if (post("createaccount") == "on") {
				$this->db->trans_start();
				// bikin member baru
				$member = array(
					"name_member" => post("name"),
					"no_member" => $this->hp(post("tel")),
					"email_member" => post("email"),
					"password_member" => post("password"),
					"status_member" => 1,
					"created_at" => date("Y-m-d H:i:s")
				);
				$add_member = $this->M_transaction->insert_table("m__member", $member);
				$last_id_member = $this->db->insert_id();
				// // simpan alamat
				$update_alamat = array(
					"status_alamat" => 0,
					"update_at" => date("Y-m-d H:i:s")
				);


				$delete = $this->M_transaction->update_table('m__alamat', $update_alamat, 'id_member', $last_id_member);
				$data_alamat = array(
					"id_member" => $last_id_member,
					"kota_alamat" => post("kota"),
					"id_kota_alamat" => post("id_kota"),
					"kecamatan_alamat" => post("kecamatan"),
					"id_kecamatan" => post("id_kecamatan"),
					"provinsi_alamat" => post("provinsi"),
					"id_provinsi_alamat" => post("id_provinsi"),
					"kode_pos_alamat" => post("kode_pos"),
					"kelurahan_alamat" => post("kelurahan"),
					"alamat_lengkap" => post("alamat_lengkap"),
					"status_alamat" => 1,
					"created_at" => date("Y-m-d H:i:s")
				);
				$add = $this->M_transaction->insert_table("m__alamat", $data_alamat);
				$last_id_alamat = $this->db->insert_id();
				// // order
				$count_data_order = $this->M_transaction->get_count_data('m__order');
				$id_order    = "TRO" . date('his') . $count_data_order;
				$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
				$dataorder = array(
					"id_order" => $id_order,
					"id_member" => $last_id_member,
					"id_alamat" => $last_id_alamat,
					"status_order" => 1,
					"total_order" => 0,
					"email_member" => post("email"),
					"status_member" => 1,
					"created_at" => date("Y-m-d H:i:s")
				);
				$dataorder = $this->M_transaction->insert_table("m__order", $dataorder);
				// // cart
				$count_data = $this->M_transaction->get_count_data('m__detail_order');
				$id_cart    = "TR" . date('his') . $count_data;
				// print_r($dataproduk[0]['harga_product'] * (int) post("stok"));
				$datcart = array(
					"id_order" => $id_order,
					"id_cart" => $id_cart,
					"id_product" => post("id_product"),
					"qty" =>  post("stok"),
					"status_cart" => 1,
					"subtotal" => $dataproduk[0]['harga_product'] * (int) post("stok"),
					"created_at" => date("Y-m-d H:i:s")
				);
				$add = $this->M_transaction->insert_table("m__detail_order", $datcart);
				// update stock
				$update_stok = array(
					"stok_product" => $dataproduk[0]['stok_product'] - (int) post("stok"),
					"update_at" => date("Y-m-d H:i:s")
				);
				$update_stok = $this->M_transaction->update_table('mr__product_detail', $update_stok, 'id_product', post("id_product"));
				$data = array(
					'id' => $last_id_member,
					'email' => post("email")
				);
				$this->session->set_userdata($data);
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE) {
					$json_data =  array(
						"result" => FALSE,
						"message" => array('head' => 'Failed', 'body' => 'Gagal Membeli barang'),
						"form_error" => $error,
						"redirect" => ''
					);
					print json_encode($json_data);
					die();
				} else {
					$json_data =  array(
						"result" => TRUE,
						"message" => array('head' => 'Success', 'body' => 'Pesanan akan di Proses,admin akan mengirim pesan kepada anda melalui whatsapp'),
						"form_error" => '',
						"redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
					);
					print json_encode($json_data);
				}
			} else {
				$this->db->trans_start();
				if ($this->session->userdata("id") == "0") {
					$dataalamat = $this->M_transaction->fetch_table('* ', 'm__alamat', 'id_member = ' . "'" . post("id_member") . "' AND status_alamat = 1", '', '', '', '', TRUE);
					$count_data_order = $this->M_transaction->get_count_data('m__order');
					$id_order    = "TRO" . date('his') . $count_data_order;
					$dataorder = array(
						"id_member" => 0,
						"id_order" => $id_order,
						"status_order" => 1,
						"total_order" => 0,
						"status_member" => 0,
						"nama_member" => post("name"),
						"tel_member" => $this->hp(post("tel")),
						"kota_alamat" => post("kota"),
						"id_kota_alamat" => post("id_kota"),
						"kecamatan_alamat" => post("kecamatan"),
						"id_kecamatan" => post("id_kecamatan"),
						"provinsi_alamat" => post("provinsi"),
						"id_provinsi_alamat" => post("id_provinsi"),
						"kode_pos_alamat" => post("kode_pos"),
						"kelurahan_alamat" => post("kelurahan"),
						"alamat_lengkap" => post("alamat_lengkap"),
						"status_alamat" => 1,
						"email_member" => post("email"),
						"created_at" => date("Y-m-d H:i:s")
					);
					$dataorder = $this->M_transaction->insert_table("m__order", $dataorder);
					// cart
					$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
					// // cart
					$count_data = $this->M_transaction->get_count_data('m__detail_order');
					$id_cart    = "TR" . date('his') . $count_data;
					$datcart = array(
						"id_order" => $id_order,
						"id_cart" => $id_cart,
						"id_product" => post("id_product"),
						"qty" =>  post("stok"),
						"status_cart" => 1,
						"subtotal" => $dataproduk[0]['harga_product'] * (int) post("stok"),
						"created_at" => date("Y-m-d H:i:s")
					);
					$add = $this->M_transaction->insert_table("m__detail_order", $datcart);
					$update_stok = array(
						"stok_product" => $dataproduk[0]['stok_product'] - (int) post("stok"),
						"update_at" => date("Y-m-d H:i:s")
					);
					$update_stok = $this->M_transaction->update_table('mr__product_detail', $update_stok, 'id_product', post("id_product"));
				} else {
					$last_id_member = $this->session->userdata("id");
					$update_alamat = array(
						"status_alamat" => 0,
						"update_at" => date("Y-m-d H:i:s")
					);
					$delete = $this->M_transaction->update_table('m__alamat', $update_alamat, 'id_member', $last_id_member);
					$data_alamat = array(
						"id_member" => $last_id_member,
						"kota_alamat" => post("kota"),
						"id_kota_alamat" => post("id_kota"),
						"kecamatan_alamat" => post("kecamatan"),
						"id_kecamatan" => post("id_kecamatan"),
						"provinsi_alamat" => post("provinsi"),
						"id_provinsi_alamat" => post("id_provinsi"),
						"kode_pos_alamat" => post("kode_pos"),
						"kelurahan_alamat" => post("kelurahan"),
						"alamat_lengkap" => post("alamat_lengkap"),
						"status_alamat" => 1,
						"created_at" => date("Y-m-d H:i:s")
					);
					$add = $this->M_transaction->insert_table("m__alamat", $data_alamat);
					$dataalamat = $this->M_transaction->fetch_table('* ', 'm__alamat', 'id_member = ' . "'" . post("id_member") . "' AND status_alamat = 1", '', '', '', '', TRUE);
					$count_data_order = $this->M_transaction->get_count_data('m__order');
					$id_order    = "TRO" . date('his') . $count_data_order;
					$dataorder = array(
						"id_member" => $last_id_member,
						"id_order" => $id_order,
						"status_order" => 1,
						"total_order" => 0,
						"status_member" => 1,
						"nama_member" => post("name"),
						"tel_member" => $this->hp(post("tel")),
						"kota_alamat" => post("kota"),
						"id_kota_alamat" => post("id_kota"),
						"kecamatan_alamat" => post("kecamatan"),
						"id_kecamatan" => post("id_kecamatan"),
						"provinsi_alamat" => post("provinsi"),
						"id_provinsi_alamat" => post("id_provinsi"),
						"kode_pos_alamat" => post("kode_pos"),
						"kelurahan_alamat" => post("kelurahan"),
						"alamat_lengkap" => post("alamat_lengkap"),
						"status_alamat" => 1,
						"email_member" => post("email"),
						"created_at" => date("Y-m-d H:i:s")
					);
					$dataorder = $this->M_transaction->insert_table("m__order", $dataorder);
					// cart
					$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
					// // cart
					$count_data = $this->M_transaction->get_count_data('m__detail_order');
					$id_cart    = "TR" . date('his') . $count_data;
					$datcart = array(
						"id_order" => $id_order,
						"id_cart" => $id_cart,
						"id_product" => post("id_product"),
						"qty" =>  post("stok"),
						"status_cart" => 1,
						"subtotal" => $dataproduk[0]['harga_product'] * (int) post("stok"),
						"created_at" => date("Y-m-d H:i:s")
					);
					$add = $this->M_transaction->insert_table("m__detail_order", $datcart);
					$update_stok = array(
						"stok_product" => $dataproduk[0]['stok_product'] - (int) post("stok"),
						"update_at" => date("Y-m-d H:i:s")
					);
					$update_stok = $this->M_transaction->update_table('mr__product_detail', $update_stok, 'id_product', post("id_product"));
				}

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE) {
					$json_data =  array(
						"result" => FALSE,
						"message" => array('head' => 'Failed', 'body' => 'Gagal Membeli barang'),
						"form_error" => $error,
						"redirect" => ''
					);
					print json_encode($json_data);
					die();
				} else {
					$json_data =  array(
						"result" => TRUE,
						"message" => array('head' => 'Success', 'body' => 'Pesanan akan di Proses,admin akan mengirim pesan kepada anda melalui whatsapp'),
						"form_error" => '',
						"redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
					);
					print json_encode($json_data);
				}
			}
		}
	}
	public function beli_tetap()
	{
		$this->form_validation->set_rules('stok', 'stok', 'required');
		$this->form_validation->set_rules('id_product', 'id_product', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$this->db->trans_start();

			$last_id_member = $this->session->userdata("id");
			$dataalamat = $this->M_transaction->fetch_table('* ', 'm__alamat', 'id_member = ' . "'" . $last_id_member . "' AND status_alamat = 1", '', '', '', '', TRUE);
			$last_id_alamat = $dataalamat[0]['id_alamat'];
			// // order
			$count_data_order = $this->M_transaction->get_count_data('m__order');
			$id_order    = "TRO" . date('his') . $count_data_order;
			$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
			$dataorder = array(
				"id_order" => $id_order,
				"id_member" => $last_id_member,
				"id_alamat" => $last_id_alamat,
				"status_order" => 1,
				"total_order" => 0,
				"status_member" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$dataorder = $this->M_transaction->insert_table("m__order", $dataorder);
			// // cart
			$count_data = $this->M_transaction->get_count_data('m__detail_order');
			$id_cart    = "TR" . date('his') . $count_data;
			// print_r($dataproduk[0]['harga_product'] * (int) post("stok"));
			$datcart = array(
				"id_order" => $id_order,
				"id_cart" => $id_cart,
				"id_product" => post("id_product"),
				"qty" =>  post("stok"),
				"status_cart" => 1,
				"subtotal" => $dataproduk[0]['harga_product'] * (int) post("stok"),
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_transaction->insert_table("m__detail_order", $datcart);
			// update stock
			$update_stok = array(
				"stok_product" => $dataproduk[0]['stok_product'] - (int) post("stok"),
				"update_at" => date("Y-m-d H:i:s")
			);
			$update_stok = $this->M_transaction->update_table('mr__product_detail', $update_stok, 'id_product', post("id_product"));
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Gagal membeli Barang'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Pesanan akan di Proses,admin akan mengirim pesan kepada anda melalui whatsapp'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
				);
				print json_encode($json_data);
			}
		}
	}

	public function get_alamat_active()
	{
		$last_id_member = $this->session->userdata("id");
		$joins = array(
			array(
				'table' => 'm__member b',
				'condition' => 'a.id_member = b.id_member',
				'jointype' => ''
			),
		);
		$dataalamat = $this->M_transaction->fetch_joins('m__alamat a ', 'a.*,b.name_member,b.no_member', $joins, 'a.id_member = ' . "'" . $last_id_member . "' AND status_alamat = 1", '', '', TRUE);
		$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
		$namaproduct = $this->M_transaction->fetch_table('* ', 'mr__product', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);

		// print_r($dataalamat);
		if (count($dataalamat) <= 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses '),
				"form_error" => '',
				"data" => $dataalamat,
				"product" => $dataproduk,
				"namaproduct" => $namaproduct
				// "redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
			);
			print json_encode($json_data);
		}
	}

	public function get_product()
	{
		$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);
		$namaproduct = $this->M_transaction->fetch_table('* ', 'mr__product', 'id_product = ' . "'" . post("id_product") . "'", '', '', '', '', TRUE);

		// print_r($dataalamat);
		if (count($dataproduk) <= 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses '),
				"form_error" => '',
				"product" => $dataproduk,
				"namaproduct" => $namaproduct
				// "redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
			);
			print json_encode($json_data);
		}
	}

	public function get_alamat_active_keranjang()
	{
		$last_id_member = $this->session->userdata("id");
		$joins = array(
			array(
				'table' => 'm__member b',
				'condition' => 'a.id_member = b.id_member',
				'jointype' => ''
			),
		);
		$dataalamat = $this->M_transaction->fetch_joins('m__alamat a ', 'a.*,b.name_member,b.no_member', $joins, 'a.id_member = ' . "'" . $last_id_member . "' AND status_alamat = 1", '', '', TRUE);
		// print_r($dataalamat);
		if (count($dataalamat) <= 0) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
				"form_error" => '',
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Sukses '),
				"form_error" => '',
				"data" => $dataalamat
				// "redirect" => '' . base_url() . $this->config->item('index_page') . '/products/' . post("id_product")
			);
			print json_encode($json_data);
		}
	}

	public function beli_keranjang()
	{

		$this->db->trans_start();

		$last_id_member = $this->session->userdata("id");
		$dataalamat = $this->M_transaction->fetch_table('* ', 'm__alamat', 'id_member = ' . "'" . $last_id_member . "' AND status_alamat = 1", '', '', '', '', TRUE);
		$last_id_alamat = $dataalamat[0]['id_alamat'];
		// // order
		$count_data_order = $this->M_transaction->get_count_data('m__order');
		$id_order    = "TRO" . date('his') . $count_data_order;
		$dataorder = array(
			"id_order" => $id_order,
			"id_member" => $last_id_member,
			"id_alamat" => $last_id_alamat,
			"status_order" => 1,
			"total_order" => 0,
			"status_member" => 1,
			"created_at" => date("Y-m-d H:i:s")
		);
		$dataorder = $this->M_transaction->insert_table("m__order", $dataorder);
		// // cart
		$data = $this->M_transaction->fetch_joins('m__cart a', '*', '', 'a.status_cart = 0 AND a.id_member=' . '\'' . $last_id_member . '\'', TRUE);
		foreach ($data as $keranjang) {
			$count_data = $this->M_transaction->get_count_data('m__detail_order');
			$dataproduk = $this->M_transaction->fetch_table('* ', 'mr__product_detail', 'id_product = ' . "'" . $keranjang["id_product"] . "'", '', '', '', '', TRUE);
			$hmproduct = $this->M_transaction->fetch_table('* ', 'mr__product', 'id_product = ' . "'" . $keranjang["id_product"] . "'", '', '', '', '', TRUE);
			if ($dataproduk[0]['stok_product'] < $keranjang["qty"]) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Mohon Maaf Stok Telah Habis'),
					"form_error" => $hmproduct[0]['name_product'],
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			}
			$id_cart    = "TR" . date('his') . $count_data;
			$datcart = array(
				"id_order" => $id_order,
				"id_cart" => $id_cart,
				"id_product" => $keranjang["id_product"],
				"qty" =>  $keranjang["qty"],
				"status_cart" => 1,
				"subtotal" => $dataproduk[0]['harga_product'] * (int) $keranjang["qty"],
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_transaction->insert_table("m__detail_order", $datcart);
			// update stock
			$update_stok = array(
				"stok_product" => $dataproduk[0]['stok_product'] - (int) $keranjang["qty"],
				"update_at" => date("Y-m-d H:i:s")
			);
			$update_stok = $this->M_transaction->update_table('mr__product_detail', $update_stok, 'id_product', $keranjang["id_product"]);

			$status_cart = array(
				"status_cart" => 1
			);
			$update_stok = $this->M_transaction->update_table('m__cart', $status_cart, 'id_member', $last_id_member);
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$json_data =  array(
				"result" => TRUE,
				"message" => array('head' => 'Success', 'body' => 'Pesanan akan di Proses,admin akan mengirim pesan kepada anda melalui whatsapp'),
				"form_error" => '',
				"redirect" => '' . base_url() . $this->config->item('index_page') . 'keranjang'
			);
			print json_encode($json_data);
		}
	}

	public function hp($nohp)
	{
		// kadang ada penulisan no hp 0811 239 345
		$nohp = str_replace(" ", "", $nohp);
		// kadang ada penulisan no hp (0274) 778787
		$nohp = str_replace("(", "", $nohp);
		// kadang ada penulisan no hp (0274) 778787
		$nohp = str_replace(")", "", $nohp);
		// kadang ada penulisan no hp 0811.239.345
		$nohp = str_replace(".", "", $nohp);
		$hp = "";

		// cek apakah no hp mengandung karakter + dan 0-9
		if (!preg_match('/[^+0-9]/', trim($nohp))) {
			// cek apakah no hp karakter 1-3 adalah +62
			if (substr(trim($nohp), 0, 3) == '+62') {
				$hp = '62' . substr(trim($nohp), 3);
			}
			// cek apakah no hp karakter 1 adalah 0
			elseif (substr(trim($nohp), 0, 1) == '0') {
				$hp = '62' . substr(trim($nohp), 1);
			} else {
				$hp = $nohp;
			}
		}
		// print($hp);
		return $hp;
	}
}
