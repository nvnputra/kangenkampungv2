<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_login');
	}


	public function ajax_action_login()
	{
		$this->form_validation->set_rules('email', 'email',  'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$form_error = $this->form_validation->error_array();
			$response =  array(
				'result' => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan data terisi semua.'),
				'form_error' => $form_error,
			);
			echo json_encode($response, JSON_PRETTY_PRINT);
			die();
		} else {
			$email = post('email');
			$password = post('password');

			$dataarr = array(
				'email_member' => $email,
				'password_member' => $password
			);

			$login = $this->db->get_where('m__member', $dataarr);
			if ($login->num_rows() > 0) {
				$sess = $login->row_array();


				$data = array(
					'id' => $sess['id_member'],
					'email' => $sess['email_member']
				);

				$this->session->set_userdata($data);

				$data = array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Berhasil'),
					"form_error" => '',
					"redirect" => base_url() . $this->config->item('index_page') . ''
				);
				echo json_encode($data);
				die();
			} else {
				$data = array(
					"result" => FALSE,
					"message" => array('head' => 'failed', 'body' => 'Pastikan email dan passwird anda benar'),
					"form_error" => '',
					"redirect" => ''
				);
				echo json_encode($data);
				die();
			}
		}
	}

	function ajax_action_logout()
	{
		$this->session->sess_destroy();
		$data = array(
			"result" => TRUE,
			"message" => array('head' => 'Success', 'body' => 'Berhasil'),
			"form_error" => '',
			"redirect" => base_url() . $this->config->item('index_page') . ''
		);
		echo json_encode($data);
		redirect(base_url() . $this->config->item('index_page') . '');
		die();
	}

	public function ajax_action_daftar()
	{
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('tel', 'tel', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('provinsi', 'provinsi', 'required');
		$this->form_validation->set_rules('kota', 'kota', 'required');
		$this->form_validation->set_rules('kecamatan', 'kecamatan', 'required');
		$this->form_validation->set_rules('kelurahan', 'kelurahan', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$this->form_validation->set_rules('kodepos', 'kodepos', 'required');
		if ($this->form_validation->run() == FALSE) {
			$error = $this->form_validation->error_array();
			$json_data =  array(
				"result" => FALSE,
				"message" => array('head' => 'Failed', 'body' => 'Pastikan Data terisi Semua'),
				"form_error" => $error,
				"redirect" => ''
			);
			print json_encode($json_data);
			die();
		} else {
			$data = array(
				"name_member" => post("name"),
				"no_member" => $this->hp(post("tel")),
				"email_member" => post("email"),
				"password_member" => post("password"),
				"status_member" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add_member = $this->M_login->insert_table("m__member", $data);
			$last_id = $this->db->insert_id();
			$data_detail = array(
				"id_member" => $last_id,
				"kota_alamat" => post("kota"),
				"id_kota_alamat" => post("id_kota"),
				"kecamatan_alamat" => post("kecamatan"),
				"id_kecamatan" => post("id_kecamatan"),
				"provinsi_alamat" => post("provinsi"),
				"id_provinsi_alamat" => post("id_provinsi"),
				"kode_pos_alamat" => post("kode_pos"),
				"kelurahan_alamat" => post("kelurahan"),
				"alamat_lengkap" => post("keterangan"),
				"status_alamat" => 1,
				"created_at" => date("Y-m-d H:i:s")
			);
			$add = $this->M_login->insert_table("m__alamat", $data_detail);
			if ($add == FALSE) {
				$json_data =  array(
					"result" => FALSE,
					"message" => array('head' => 'Failed', 'body' => 'Gagal Menambah Data'),
					"form_error" => $error,
					"redirect" => ''
				);
				print json_encode($json_data);
				die();
			} else {
				$data = array(
					'id' => $last_id,
					'email' => post("email")
				);
				$this->session->set_userdata($data);
				$json_data =  array(
					"result" => TRUE,
					"message" => array('head' => 'Success', 'body' => 'Sukses Mengisi data'),
					"form_error" => '',
					"redirect" => '' . base_url() . $this->config->item('index_page') . ''
				);
				print json_encode($json_data);
			}
		}
	}
	public function hp($nohp)
	{
		// kadang ada penulisan no hp 0811 239 345
		$nohp = str_replace(" ", "", $nohp);
		// kadang ada penulisan no hp (0274) 778787
		$nohp = str_replace("(", "", $nohp);
		// kadang ada penulisan no hp (0274) 778787
		$nohp = str_replace(")", "", $nohp);
		// kadang ada penulisan no hp 0811.239.345
		$nohp = str_replace(".", "", $nohp);
		$hp = "";

		// cek apakah no hp mengandung karakter + dan 0-9
		if (!preg_match('/[^+0-9]/', trim($nohp))) {
			// cek apakah no hp karakter 1-3 adalah +62
			if (substr(trim($nohp), 0, 3) == '+62') {
				$hp = '62' . substr(trim($nohp), 3);
			}
			// cek apakah no hp karakter 1 adalah 0
			elseif (substr(trim($nohp), 0, 1) == '0') {
				$hp = '62' . substr(trim($nohp), 1);
			} else {
				$hp = $nohp;
			}
		}
		// print($hp);
		return $hp;
	}
}
