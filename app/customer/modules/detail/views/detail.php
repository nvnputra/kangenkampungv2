<!-- SECTION -->
<?php $imgHeight = "style='height:150px;'";
if (!$this->agent->is_mobile()) {
    $imgHeight = 'style="height:270px;"';
    $paddingButton = '';
    $paddingButton2 = '';
} else {
    $imgHeight = "";
    $paddingButton = 'style="padding:10px!important;font-size:12px!important;"';
    $paddingButton2 = 'padding:10px!important;font-size:12px!important;';
}
?>
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Product main img -->
            <div class="col-md-5 col-md-push-2">
                <div id="product-main-img">
                    <?php foreach ($images as $key) { ?>
                        <div class="product-preview">
                            <img src="<?php echo base_url() . "uploads/product/" . $key['name_file']; ?>" alt="">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /Product main img -->

            <!-- Product thumb imgs -->
            <div class="col-md-2  col-md-pull-5" id="imgs">
                <div id="product-imgs">
                    <!--  -->
                    <?php foreach ($images as $key) { ?>
                        <div class="product-preview">
                            <img src="<?php echo base_url() . "uploads/product/" . $key['name_file']; ?>" alt="">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /Product thumb imgs -->

            <!-- Product details -->
            <div class="col-md-5">
                <div class="product-details">
                    <h2 class="product-name" id="product_name"></h2>
                    <div>
                        <div class="product-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <label id="name_merchant"></label>
                    </div>
                    <div>
                        <h3 class="product-price" id="price"><del class="product-old-price" id="diskon"></del></h3>
                        <span class="product-available" id="stock"></span>
                    </div>
                    <p id="catatan_product"></p>

                    <div class="product-options" style="display: none">
                        <label>
                            Size
                            <select class="input-select">
                                <option value="0">X</option>
                            </select>
                        </label>
                        <label>
                            Color
                            <select class="input-select">
                                <option value="0">Red</option>
                            </select>
                        </label>
                    </div>

                    <div class="add-to-cart">
                        <div class="qty-label">
                            Qty
                            <div class="input-number">
                                <input type="number" value="1" id="qty">
                                <span class="qty-up" onclick="qtyListener()">+</span>
                                <span class="qty-down" onclick="qtyListener()">-</span>
                            </div>
                        </div>
                        <?php if (!($this->session->userdata("id"))) { ?>
                            <button class="add-to-cart-btn" data-toggle="modal" data-target="#buy-modal"><i class="fa fa-shopping-cart"></i> Pesan Barang</button>
                        <?php } else { ?>
                            <!-- <button class="add-to-cart-btn" data-toggle="modal" data-target="#pilih_buy"><i class="fa fa-shopping-cart"></i>Pesan Barang</button> -->

                            <button class="primary-btn order-submit" data-toggle="modal" data-target="#pilih_buy">Beli</button>
                            <button class="primary-btn order-submit" data-toggle="modal" onclick="tambah_ke_keranjang()"><i class="fa fa-shopping-cart fa-lg"></i></button>

                        <?php } ?>
                    </div>
                    <ul>
                        <div style="color:red">* Harga tidak termasuk ongkos kirim</div>
                    </ul>
                    <ul class="product-btns" style="display:none">
                        <li><a href="#"><i class="fa fa-heart-o"></i> add to wishlist</a></li>
                        <li><a href="#"><i class="fa fa-exchange"></i> add to compare</a></li>
                    </ul>

                    <ul class="product-links">
                        <li>Category:</li>
                        <li id="category"></li>
                    </ul>

                    <ul class="product-links" style="display: none">
                        <li>Share:</li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                    </ul>

                </div>
            </div>
            <!-- /Product details -->

            <!-- Product tab -->
            <div class="col-md-12">
                <div id="product-tab">
                    <!-- product tab nav -->
                    <ul class="tab-nav">
                        <li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
                        <!-- <li><a data-toggle="tab" href="#tab3">Reviews (3)</a></li> -->
                    </ul>
                    <!-- /product tab nav -->

                    <!-- product tab content -->
                    <div class="tab-content">
                        <!-- tab1  -->
                        <div id="tab1" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <p id="keterangan"></p>
                                </div>
                            </div>
                        </div>
                        <!-- /tab1  -->

                        <!-- tab2  -->
                        <div id="tab2" class="tab-pane fade in">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /tab2  -->

                        <!-- tab3  -->
                        <div id="tab3" class="tab-pane fade in">
                            <div class="row">
                                <!-- Rating -->
                                <div class="col-md-3">
                                    <div id="rating">
                                        <div class="rating-avg">
                                            <span>4.5</span>
                                            <div class="rating-stars">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                        </div>
                                        <ul class="rating">
                                            <li>
                                                <div class="rating-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <div class="rating-progress">
                                                    <div style="width: 80%;"></div>
                                                </div>
                                                <span class="sum">3</span>
                                            </li>
                                            <li>
                                                <div class="rating-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="rating-progress">
                                                    <div style="width: 60%;"></div>
                                                </div>
                                                <span class="sum">2</span>
                                            </li>
                                            <li>
                                                <div class="rating-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="rating-progress">
                                                    <div></div>
                                                </div>
                                                <span class="sum">0</span>
                                            </li>
                                            <li>
                                                <div class="rating-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="rating-progress">
                                                    <div></div>
                                                </div>
                                                <span class="sum">0</span>
                                            </li>
                                            <li>
                                                <div class="rating-stars">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                                <div class="rating-progress">
                                                    <div></div>
                                                </div>
                                                <span class="sum">0</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Rating -->

                                <!-- Reviews -->
                                <div class="col-md-6">
                                    <div id="reviews">
                                        <ul class="reviews">
                                            <li>
                                                <div class="review-heading">
                                                    <h5 class="name">John</h5>
                                                    <p class="date">27 DEC 2018, 8:0 PM</p>
                                                    <div class="review-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o empty"></i>
                                                    </div>
                                                </div>
                                                <div class="review-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="review-heading">
                                                    <h5 class="name">John</h5>
                                                    <p class="date">27 DEC 2018, 8:0 PM</p>
                                                    <div class="review-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o empty"></i>
                                                    </div>
                                                </div>
                                                <div class="review-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="review-heading">
                                                    <h5 class="name">John</h5>
                                                    <p class="date">27 DEC 2018, 8:0 PM</p>
                                                    <div class="review-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o empty"></i>
                                                    </div>
                                                </div>
                                                <div class="review-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="reviews-pagination">
                                            <li class="active">1</li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Reviews -->

                                <!-- Review Form -->
                                <div class="col-md-3">
                                    <div id="review-form">
                                        <form class="review-form">
                                            <input class="input" type="text" placeholder="Your Name">
                                            <input class="input" type="email" placeholder="Your Email">
                                            <textarea class="input" placeholder="Your Review"></textarea>
                                            <div class="input-rating">
                                                <span>Your Rating: </span>
                                                <div class="stars">
                                                    <input id="star5" name="rating" value="5" type="radio"><label for="star5"></label>
                                                    <input id="star4" name="rating" value="4" type="radio"><label for="star4"></label>
                                                    <input id="star3" name="rating" value="3" type="radio"><label for="star3"></label>
                                                    <input id="star2" name="rating" value="2" type="radio"><label for="star2"></label>
                                                    <input id="star1" name="rating" value="1" type="radio"><label for="star1"></label>
                                                </div>
                                            </div>
                                            <button class="primary-btn">Submit</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /Review Form -->
                            </div>
                        </div>
                        <!-- /tab3  -->
                    </div>
                    <!-- /product tab content  -->
                </div>
            </div>
            <!-- /product tab -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /SECTION -->

<!-- Section -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">

            <div class="col-md-12">
                <div class="section-title text-center">
                    <h3 class="title">Lihat Juga Produk ini</h3>
                </div>
            </div>
            <div id="div_product">
            </div>
            <div class="clearfix visible-sm visible-xs"></div>




        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /Section -->
<input style="display: none" id="stock_in" type="text">

<script type="text/javascript">
    $(document).ready(function() {
        ajax_get_product_detail();
    });

    function tambah_ke_keranjang() {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>transaction/get_alamat_active";
        var html = "";
        var form_data = new FormData();
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            html = `<table class="table table-bordered">
            <tr>
                                                        <td>Nama</td>
                                                        <td>${response.data[0].name_member}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No Hp</td>
                                                        <td>${response.data[0].no_member}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Barang</td>
                                                        <td>${response.namaproduct[0].name_product}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>${addNumber(response.product[0].harga_product,true)}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah</td>
                                                        <td>${$('#qty').val()}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat lengkap</td>
                                                        <td>${response.data[0].alamat_lengkap}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>${addNumber((response.product[0].harga_product * parseInt($('#qty').val())),true)}</td>
                                                    </tr>
                                                </table>`;

            $('#tambah-keranjang').html(html);
            $('#modal-tambah-keranjang').modal("show");
        });
    }

    function qtyListener() {
        if (parseInt($('#qty').val()) == (parseInt($('#stock_in').val()) - 1)) {
            $('.qty-up').css("display", 'none');
        } else {
            $('.qty-up').css("display", 'block');
            // alert("Bbbb");
        }
    }

    function ajax_get_product_detail() {
        var link = "<?php echo base_url() . $this->config->item('index_page'); ?>product/get_data_product_by_id/";
        var image_link = "<?php echo base_url(); ?>/uploads/product/";
        var form_data = new FormData();
        var trHTML = '';
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link, "POST", form_data, function(response) {
            var diskon = parseInt(response.data.product[0].diskon_product) / 100 * response.data.product[0].harga_product;
            var diskon_total = response.data.product[0].harga_product - diskon;
            if (response.data.product[0].diskon_product > 0) {
                var discount_price = addNumber(response.data.product[0].harga_product, true);
                var real_price = addNumber(diskon_total, true);
            } else {
                var discount_price = "";
                var real_price = addNumber(response.data.product[0].harga_product, true);
            }
            $('#product_name').text(response.data.product[0].name_product);
            $('#name_merchant').text(response.data.product[0].name_merchant);
            $('#price').text(real_price);
            $('#diskon').text(response.data.product[0].name_merchant);
            $('#stock').text(response.data.product[0].stok_product + " PCS");
            $('#stock_in').val(response.data.product[0].stok_product);
            $('#category').text(response.data.product[0].name_category);
            $('#catatan_product').html(response.data.product[0].catatan_product);
            $('#keterangan').html(response.data.product[0].informasi_product);
            get_data_product_by_all_category(response.data.product[0].name_category);
        });

    }
</script>

<script type="text/javascript">
    function get_data_product_by_all_category(category) {
        var i, j, x = "";
        var link = "<?php echo base_url() . index_page(); ?>product/get_data_product_by_all_category/";
        var form_data = new FormData();
        form_data.append('category', category);
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        var html = "";
        ajaxShowData(link, "POST", form_data, function(response) {
            if (response.data.length > 4) {
                var panjang = 4;
            } else {
                var panjang = response.data.length;
            }
            for (i = 0; i < panjang; i++) {
                html += '<div class="col-xs-6 col-sm-3">' +
                    '<div class="product">' +
                    '<div class="product-img">' +
                    '<img <?= $imgHeight ?> src="<?php echo base_url() . "uploads/product/"; ?>' + response.data[i].name_file + '" alt="">' +
                    '<div class="product-label">' +
                    // '<span class="sale">-30%</span>' +
                    // '<span class="new">NEW</span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="product-body">' +
                    '<p class="product-category">' + response.data[i].name_merchant + '</p>' +
                    '<h3 class="product-name">' + response.data[i].name_product + '</h3>' +
                    '<h4 class="product-price">' + addNumber(response.data[i].harga_product, true) + '</h4>' +
                    '</div>' +
                    '<div style="text-align:center">' +
                    '<a href="<?php echo base_url() . index_page() . 'products/'; ?>' + response.data[i].id_product + '"> <button class="primary-btn order-submit" <?= $paddingButton ?>><i class="fa fa-shopping-cart"></i> Lihat Produk</button></a>' +
                    `<button class="primary-btn order-submit" style="margin-top: 5px;<?= $paddingButton2 ?>" onClick="chat('Saya Ingin bertanya tentang produk ${response.data[i].name_product} ini ?')"><i class="fa fa-whatsapp"></i> Tanya ` +
                    'Admin</button>' +
                    '</div>' +
                    '<br></div>' +
                    '</div>';
            }
            $('#div_product').html(html);

        });

    }

    function tambah_keranjang() {
        var form_data = new FormData();
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('stok', $('#qty').val());
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerializePesan("<?php echo base_url() . $this->config->item('index_page'); ?>keranjang/tambah_keranjang/", "POST", form_data);
    }
</script>