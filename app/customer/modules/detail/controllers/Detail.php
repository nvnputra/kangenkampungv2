<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detail extends MY_Controller
{

	public function __construct()
	{
		$this->load->model('M_detail');
	}

	public function home($param)
	{
		$data['content']  = 'detail';
		$id              = $param;
		$joins           = array(
			array(
				'table' => 'mr__product_detail b',
				'condition' => 'a.id_product = b.id_product',
				'jointype' => ''
			),
			array(
				'table' => 'a__category c',
				'condition' => 'a.id_kategory = c.id_category',
				'jointype' => ''
			)
		);
		// $data['product'] = $this->M_product->fetch_joins('mr__product a', '*', $joins, 'a.id_product = ' . "'" . $id . "'", '', TRUE);
		$data['images']   = $this->M_detail->fetch_table('name_file ', 'mr__file', 'id_relation = ' . "'" . $id . "'", '', '', '', '', TRUE);
		$this->load->view('template', $data);
	}
}
