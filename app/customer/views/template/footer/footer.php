<!-- FOOTER -->
<!-- NEWSLETTER -->
<div id="newsletter" class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="newsletter">
                    <p>Untuk Informasi Lebih Lanjut <strong></strong></p>
                    <form onsubmit="return false">
                        <input class="input" type="text" placeholder="Ketik Pesan Di Sini" id="pesan_admin">
                        <button class="newsletter-btn" onclick='chat_footer()'><i class="fa fa-whatsapp"></i> Chat wa</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<footer id="footer">
    <!-- top footer -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <center>
                    <div class="col-md-12 col-xs-12">
                        <div class="footer">
                            <h3 class="footer-title">About Us</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
                            <ul class="footer-links">
                                <li><a href="#"><i class="fa fa-map-marker"></i>Jl. Tumenggung Suryo No. 135, Kota Malang</a></li>
                                <li><a href="#"><i class="fa fa-phone"></i>+6282244109977</a></li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i>kangenkampung.id@gmail.com</a></li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i>haiadmin@kangenkampung.id</a></li>
                            </ul>
                        </div>
                    </div>
                </center>

                <!-- <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Categories</h3>
                        <ul class="footer-links">
                            <li><a href="#">Hot deals</a></li>
                            <li><a href="#">Laptops</a></li>
                            <li><a href="#">Smartphones</a></li>
                            <li><a href="#">Cameras</a></li>
                            <li><a href="#">Accessories</a></li>
                        </ul>
                    </div>
                </div>

                <div class="clearfix visible-xs"></div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Information</h3>
                        <ul class="footer-links">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Orders and Returns</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </div>fcategory_search
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Service</h3>
                        <ul class="footer-links">
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">View Cart</a></li>
                            <li><a href="#">Wishlist</a></li>
                            <li><a href="#">Track My Order</a></li>
                            <li><a href="#">Help</a></li>
                        </ul>
                    </div>
                </div> -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /top footer -->

    <!-- bottom footer -->

    <!-- /bottom footer -->
</footer>
<!-- /FOOTER -->

<!-- jQuery Plugins -->

<script type="text/javascript">
    // A $( document ).ready() block.
    $(document).ready(function() {
        ajax_action_get_all_category();
    });

    function ajax_action_get_all_category() {
        var link = "<?php echo base_url() . index_page(); ?>category/ajax_action_get_all_category/";
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        var html = '';
        var html2 = '<option value="0">All</option>';
        var html3 = ' <li class="active"><a href="<?php echo base_url() . index_page(); ?>">Home</a></li>';
        var a;
        var b;
        ajaxShowData(link, "POST", form_data, function(response) {
            for (i = 0; i < response.data.length; i++) {
                html += ' <div class="col-md-4 col-xs-6">' +
                    '<div class="shop">' +
                    '<div class="shop-img">' +
                    '<img style="height: 270px" src="<?= base_url(); ?>' + '/uploads/category/' + response.data[i].nama_file + '" alt="">' +
                    '</div>' +
                    '<div class="shop-body">' +
                    '<h3>' + response.data[i].name_category + '<br></h3>' +
                    "<a href='<?php echo base_url() . index_page() . 'productc/'; ?>" + response.data[i].name_category + "/a" + "' class='cta-btn'>Lihat " + response.data[i].name_category + " <i class='fa fa-arrow-circle-right'></i></a>" +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }
            for (a = 0; a < response.data.length; a++) {
                html2 += "<option value='" + response.data[a].name_category + "'>" + response.data[a].name_category + "</option>";
                html3 += "<li><a href='<?php echo base_url() . index_page() . 'productc/'; ?>" + response.data[a].name_category + "/a" + "'>" + response.data[a].name_category + "</a></li>";
            }
            $('#banner_product').html(html);
            $('#category_search').html(html2);
            $('#ul_category').html(html3);

        });
    }

    function cari() {
        location.href = "<?php echo base_url() . index_page() . 'productc/'; ?>" + $('#category_search option:selected').val() + "/" + $('#search').val();

    }

    function chat(pesan) {
        var number = "6282244109977";
        var text = `Hai admin,`
        window.open(
            `https://wa.me/${number}?text=` + encodeURIComponent(text + pesan),
            '_blank' // <- This is what makes it open in a new window.
        );

    }

    function chat_footer() {
        var number = "6282244109977";
        var text = `Hai admin,${$('#pesan_admin').val()}`;
        window.open(
            `https://wa.me/${number}?text=` + encodeURIComponent(text),
            '_blank' // <- This is what makes it open in a new window.
        );

    }
</script>
<script src="<?php echo base_url(); ?>assets/customer/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/slick.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/nouislider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/jquery.zoom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/customer/js/main.js"></script>
<script src="<?php echo base_url(); ?>assets/ajaxFunction.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164118566-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-164118566-1');
</script>

</body>

</html>