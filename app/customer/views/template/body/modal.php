<!-- Login Modal -->
<div class="modal fade" id="login-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Login</h3>
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" name="first-name" placeholder="Masukkan Emal" id="email">
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="password" name="last-name" placeholder="Masukkan Password" id="password">
                                        </div>
                                        <div>
                                            <a href="#" class="primary-btn order-submit" style="width: 100%" onclick="login()">Login</a>
                                            <div style="margin-top: 5px"></div>
                                            <a href="#" class="primary-btn order-submit" style="width: 100%;background: #15161d" data-toggle="modal" data-target="#daftar-modal">Daftar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- End Modal Login -->


<!-- daftar Modal -->
<div class="modal fade" id="daftar-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Form Pendaftaran</h3>
                                        </div>

                                        <div class="form-group">
                                            <input class="input" type="text" name="name" id="name" placeholder="Masukkan nama">
                                        </div>

                                        <div class="form-group">
                                            <input class="input" type="tel" name="tel" id="tel" placeholder="Nomor HP yang terdapat whatsaap">
                                        </div>

                                        <div class="form-group">
                                            <input class="input" type="email" name="email" id="emails" placeholder="Email">
                                        </div>

                                        <div class="form-group">
                                            <input class="input" type="password" name="password" placeholder="Enter Your Password" id="passwords">
                                        </div>

                                        <div class="form-group">
                                            <select id="provinsi" class="input">

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select id="kota" class="input">
                                                <option>Silahkan pilih provinsi terlebih dahulu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select id="kecamatan" class="input">
                                                <option>Silahkan pilih kota terlebih dahulu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" name="keluarahan" id="kelurahan" placeholder="Desa/kelurahan">
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" name="keterangan" id="keterangan_daftar" placeholder="Masukkan Alamat Lengkap anda | nama jalan | nomor rumah | petunjuk lengkap">
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" name="kodepos" id="kodeposs" placeholder="Kode Pos">
                                        </div>

                                        <div>
                                            <a href="#" class="primary-btn order-submit" style="width: 100%" onclick="daftar()">Daftar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- End daftar modal -->

<!-- Buy Modal -->
<div class="modal fade" id="buy-modal" role="dialog" style="z-index: 10000">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Form Pemesanan</h3>
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" id="name_beli" placeholder="Masukkan Nama Anda">
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="email" id="email_beli" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <select id="provinsi_beli" class="input">

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select id="kota_beli" class="input">
                                                <option>Silahkan pilih provinsi terlebih dahulu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select id="kecamatan_beli" class="input">
                                                <option>Silahkan pilih kota terlebih dahulu</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" id="kelurahan_beli" placeholder="Desa/kelurahan">
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" id="keterengan_beli" placeholder="Nama Gang | nama jalan | nomor rumah | petunjuk lengkap">
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" id="kodepos_beli" placeholder="Kode Pos">
                                        </div>
                                        <div class="form-group">
                                            <label for="">
                                                <span></span>
                                                Nomor Whatsappp
                                            </label>
                                            <input class="input" type="number" id="tel_beli" placeholder="08XXXXXXXX">
                                        </div>
                                        <?php if (!($this->session->userdata("id"))) { ?>
                                            <div class="form-group">
                                                <div class="input-checkbox">
                                                    <input type="checkbox" id="create-account">
                                                    <label for="create-account">
                                                        <span></span>
                                                        Buat Akun?
                                                        <p>Dengan buat Akun Anda tidak perlu mengisi form lagi untuk belanja selanjutnya</p>
                                                    </label>
                                                    <div class="caption">
                                                        <p>Apabila anda ingin membuat akun di kangenkampung silahkan isi kolom password</p>
                                                        <input class="input" type="password" name="password" name="password_beli" placeholder="Enter Your Password">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="form-group" style="display:none">
                                                <div class="input-checkbox">
                                                    <input type="checkbox" id="create-account">
                                                    <label for="create-account">
                                                        <span></span>
                                                        Buat Akun?
                                                        <p>Dengan buat Akun Anda tidak perlu mengisi form lagi untuk belanja selanjutnya</p>
                                                    </label>
                                                    <div class="caption">
                                                        <p>Apabila anda ingin membuat akun di kangenkampung silahkan isi kolom password</p>
                                                        <input class="input" type="password" name="password" name="password_beli" placeholder="Enter Your Password">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div>
                                            <a href="#" class="primary-btn order-submit" style="width: 100%" onclick="detail_pembelian()">Pesan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- End But Login -->

<!-- Tracking modal -->
<div class="modal fade" id="tracking-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Tracking Pesanan</h3>
                                        </div>
                                        <div class="form-group">
                                            <input class="input" type="text" id="track" placeholder="Masukkan kode pesanan anda di sini">
                                        </div>
                                        <div>
                                            <a href="#" class="primary-btn order-submit" style="width: 100%" onclick="tracking()">Cari barang</a>
                                            <div style="margin-top: 5px"></div>
                                            <!-- Wishlist -->
                                            <?php if ($this->session->userdata("id") != 0) { ?>
                                                <a href="<?php echo base_url() . index_page(); ?>keranjang" class="primary-btn order-submit" style="width: 100%;background: #15161d">Buka Keranjang</a>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- End Tracking modal -->
<div class="modal fade" id="logout-modal" role="dialog">
    <?php if (!$this->agent->is_mobile()) { ?>
        <div class="modal-dialog modal-dialog-centered" style="width: 20%">
        <?php } ?>
        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <center>
                                                <h3 class="title">logout</h3>
                                            </center>
                                        </div>
                                        <div>
                                            <center>
                                                <p>Terimakasih telah bekerjasama dengan Kami</p>
                                            </center>
                                        </div>
                                        <center>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <a href="<?php echo base_url() . index_page(); ?>/login/ajax_action_logout"><button class="primary-btn order-submit">keluarkan Akun</button></a>
                                                </div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        </div>
</div>

<div class="modal fade" id="pilih_buy" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Pemilihan Pembelian</h3>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="#" class="primary-btn order-submit" style="width: 100%" onclick="set_alamat_active()">Alamat Tetap</a>
                                            </div>
                                            <?php if ($this->agent->is_mobile()) { ?>
                                                <br>
                                            <?php } ?>
                                            <div class="col-sm-6">
                                                <a href="#" class="primary-btn order-submit" style="width: 100%;background:#15161d" data-toggle="modal" data-target="#buy-modal">Alamat Baru</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="alamat_tetap" role="dialog" style="z-index: 10000">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Detail Pembelian</h3>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <div id="table_active">

                                                </div>
                                                <div style="color:red;text-align: right">* Harga tidak termasuk ongkos kirim</div>

                                            </div>

                                            <div style="margin-top: 5px">
                                                <a href="#" class="primary-btn order-submit" style="width:100%" onclick="beli_alamat_tetap()">Pesan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="detail_pembelian" role="dialog" style="z-index: 10000">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Detail Pembelian</h3>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <div id="detail_table_active">

                                                </div>
                                                <div style="color:red;text-align: right">* Harga tidak termasuk ongkos kirim</div>

                                            </div>

                                            <div style="margin-top: 5px">
                                                <a href="#" class="primary-btn order-submit" style="width:100%" onclick="beli()">Pesan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal-alert-text" role="dialog" style="z-index: 10000">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title" id="title_alert"></h3>
                                        </div>
                                        <div id="body_alert">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class=" modal fade" id="modal-tambah-keranjang" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Tambah Ke keranjang</h3>
                                        </div>
                                        <div id="tambah-keranjang">
                                        </div>
                                    </div>
                                    <center>
                                        <div style="margin-top: 5px">
                                            <a href="#" class="primary-btn order-submit" style="width:100" onclick="tambah_keranjang()">Masukkan ke Keranjang</a>
                                        </div>
                                    </center>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="alamat_tetap_keranjang" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->

        <div class="modal-content" style="padding: 15px">
            <div class="card-modal">
                <div class="modal-body">
                    <div class="login-modal-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="body">
                                    <div class="billing-details">
                                        <div class="section-title">
                                            <h3 class="title">Detail Pembelian</h3>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <div id="table_active_keranjang">

                                                </div>
                                                <div style="color:red;text-align: right">* Harga tidak termasuk ongkos kirim</div>

                                            </div>

                                            <div style="margin-top: 5px">
                                                <a href="#" class="primary-btn order-submit" style="width:100%" onclick="beli_alamat_keranjang()">Pesan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<script>
    function daftar() {
        var form_data = new FormData();
        form_data.append('name', $('#name').val());
        form_data.append('tel', $('#tel').val());
        form_data.append('email', $('#emails').val());
        form_data.append('password', $('#passwords').val());
        form_data.append('provinsi', $('#provinsi option:selected').text());
        form_data.append('kota', $('#kota option:selected').text());
        form_data.append('kecamatan', $('#kecamatan option:selected').text());
        form_data.append('id_provinsi', $('#provinsi').val());
        form_data.append('id_kota', $('#kota').val());
        form_data.append('id_kecamatan', $('#kecamatan').val());
        form_data.append('kelurahan', $('#kelurahan').val());
        form_data.append('keterangan', $('#keterangan_daftar').val());
        form_data.append('kodepos', $('#kodeposs').val());
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>login/ajax_action_daftar/", "POST", form_data);
    }
</script>


<script>
    function beli() {
        $('#detail_pembelian').modal('hide');
        $('#buy-modal').modal('hide');
        $('#alamat_tetap_keranjang').modal("hide");
        var chk = "0";
        var remember = document.getElementById("create-account");
        if (remember.checked) {
            chk = "on";
        } else {
            chk = "off";
        }
        // alert(chk);
        var form_data = new FormData();
        form_data.append('name', $('#name_beli').val());
        form_data.append('tel', $('#tel_beli').val());
        form_data.append('email', $('#email_beli').val());
        form_data.append('password', $('#password_beli').val());
        form_data.append('provinsi', $('#provinsi_beli option:selected').text());
        form_data.append('kota', $('#kota_beli option:selected').text());
        form_data.append('kecamatan', $('#kecamatan_beli option:selected').text());
        form_data.append('id_provinsi', $('#provinsi_beli').val());
        form_data.append('id_kota', $('#kota_beli').val());
        form_data.append('id_kecamatan', $('#kecamatan_beli').val());
        form_data.append('kelurahan', $('#kelurahan_beli').val());
        form_data.append('alamat_lengkap', $('#keterengan_beli').val());
        form_data.append('kodepos', $('#kodepos_beli').val());
        form_data.append('createaccount', chk);
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('stok', $('#qty').val());
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerializePesan("<?php echo base_url() . $this->config->item('index_page'); ?>transaction/beli_now/", "POST", form_data);
    }
</script>

<script>
    function beli_alamat_tetap() {
        $('#alamat_tetap').modal('hide');
        $('#detail_pembelian').modal("hide");
        var form_data = new FormData();
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('stok', $('#qty').val());
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerializePesan("<?php echo base_url() . $this->config->item('index_page'); ?>transaction/beli_tetap/", "POST", form_data);
    }
</script>

<script>
    function beli_alamat_keranjang() {
        $('#alamat_tetap').modal('hide');
        $('#detail_pembelian').modal("hide");
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerializePesan("<?php echo base_url() . $this->config->item('index_page'); ?>transaction/beli_keranjang/", "POST", form_data);
    }
</script>






<script>
    function login() {
        var form_data = new FormData();
        form_data.append('email', $('#email').val());
        form_data.append('password', $('#password').val());
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        addItemSerialize("<?php echo base_url() . $this->config->item('index_page'); ?>login/ajax_action_login/", "POST", form_data);
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        set_provinci();
    });
</script>
<script type="text/javascript">
    function set_provinci() {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
        $('#page-load').show();
        var form_data = new FormData();
        form_data.append('param', "province");
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            var html = '<option value="" selected="selected">Pilih Provinsi</option>';
            var i;
            for (i = 0; i < response.rajaongkir.results.length; i++) {
                html += "<option value='" + response.rajaongkir.results[i].province_id + "'>" + response.rajaongkir.results[i].province + "</option>";
            }
            $('#provinsi').html(html);
            $('#provinsi_beli').html(html);
            $('#page-load').hide();
        });
    }
    $('#provinsi').on('change', function() {
        set_kota_detail(this.value);
        $('#page-load').hide();
    });
    $('#provinsi_beli').on('change', function() {
        set_kota_detail(this.value);
        $('#page-load').hide();
    });

    function set_kota_detail(value) {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
        $('#page-load').show();
        var form_data = new FormData();
        form_data.append('param', "city?province=" + value);
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            ;
            var html = '<option value="" selected="selected">Pilih Kota</option>';
            var i;
            for (i = 0; i < response.rajaongkir.results.length; i++) {
                html += "<option value='" + response.rajaongkir.results[i].city_id + "'>" + response.rajaongkir.results[i].city_name + "</option>";
            }
            $('#kota').html(html);
            $('#kota_beli').html(html);
            $('#page-load').hide();
        });
    }
    $('#kota').on('change', function() {
        set_kecamatan_detail(this.value);
        $('#page-load').hide();
    });
    $('#kota_beli').on('change', function() {
        set_kecamatan_detail(this.value);
        $('#page-load').hide();
    });

    function set_kecamatan_detail(value) {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>rajaongkir/get_provinci_ongkir";
        $('#page-load').show();
        var form_data = new FormData();
        form_data.append('param', "subdistrict?city=" + value);
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            ;
            var html = '<option value="" selected="selected">Pilih Kecamatan</option>';
            var i;
            for (i = 0; i < response.rajaongkir.results.length; i++) {
                html += "<option value='" + response.rajaongkir.results[i].subdistrict_id + "'>" + response.rajaongkir.results[i].subdistrict_name + "</option>";
            }
            $('#kecamatan').html(html);
            $('#kecamatan_beli').html(html);
            $('#page-load').hide();
        });
    }

    function set_alamat_active() {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>transaction/get_alamat_active";
        var html = "";
        var form_data = new FormData();
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            html = `<table class="table table-bordered">
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td>${response.data[0].name_member}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No Hp</td>
                                                        <td>${response.data[0].no_member}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Barang</td>
                                                        <td>${response.namaproduct[0].name_product}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>${addNumber(response.product[0].harga_product,true)}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah</td>
                                                        <td>${$('#qty').val()}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat lengkap</td>
                                                        <td>${response.data[0].alamat_lengkap}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>${addNumber((response.product[0].harga_product * parseInt($('#qty').val())),true)}</td>
                                                    </tr>
                                                </table>`;

            $('#table_active').html(html);
            $('#alamat_tetap').modal("show");
        });


    }

    function detail_pembelian() {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>transaction/get_product";
        var html = "";
        var form_data = new FormData();
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            html = `<table class="table table-bordered">
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td>${$('#name_beli').val()}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No Hp</td>
                                                        <td>${$('#tel_beli').val()}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Barang</td>
                                                        <td>${response.namaproduct[0].name_product}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>${addNumber(response.product[0].harga_product,true)}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jumlah</td>
                                                        <td>${$('#qty').val()}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat lengkap</td>
                                                        <td>
                                                        ${$('#provinsi_beli option:selected').text()} 
                                                        ${$('#kota_beli option:selected').text()} 
                                                        ${$('#kecamatan_beli option:selected').text()} 
                                                        ${$('#provinsi').val()}
                                                        ${$('#kelurahan_beli').val()}
                                                        ${$('#keterengan_beli').val()}
                                                        ${$('#kodepos_beli').val()}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Harga</td>
                                                        <td>${addNumber((response.product[0].harga_product * parseInt($('#qty').val())),true)}</td>
                                                    </tr>
                                                </table>`;

            $('#detail_table_active').html(html);
            $('#detail_pembelian').modal("show");
        });
    }

    function set_alamat_keranjang() {
        var link2 = "<?php echo base_url() . $this->config->item('index_page'); ?>transaction/get_alamat_active_keranjang";
        var html = "";
        var form_data = new FormData();
        form_data.append('id_product', '<?= $this->uri->segment(2); ?>');
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');
        ajaxShowData(link2, "POST", form_data, function(response) {
            html = `<table class="table table-bordered">
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td>${response.data[0].name_member}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No Hp</td>
                                                        <td>${response.data[0].no_member}</td>
                                                    </tr>
                                                    <tr>
                                                    <tr>
                                                        <td>Alamat lengkap</td>
                                                        <td>
                                                        ${response.data[0].provinsi_alamat}
                                                        ${response.data[0].kota_alamat}
                                                        ${response.data[0].kecamatan_alamat}
                                                        ${response.data[0].kelurahan_alamat}
                                                        ${response.data[0].kode_pos_alamat}
                                                        ${response.data[0].alamat_lengkap}
                                                        </td>
                                                    </tr>
                                                    </tr>
                                                    <tr>
                                                </table>`;

            $('#table_active_keranjang').html(html);
            $('#alamat_tetap_keranjang').modal("show");
        });
    }

    function tracking() {
        location.href = "<?php echo base_url() . index_page() . 'tracking/'; ?>" + $('#track').val();

    }
</script>