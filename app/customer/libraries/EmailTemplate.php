<?php
class EmailTemplate {
	private $dir_temp = '/shared_libraries/email/reseller/';	
	
	public function test($url,$data_user) {
		ob_start();
		//require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.bak.php');
		require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.php');
		return ob_get_clean();
    }

    public function signup($url,$data_user) {
		ob_start();
		//require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'signup.bak.php');
		 require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'signup.php');
		return ob_get_clean();
    }
     public function forgot($url,$data_user) {
		ob_start();
		//require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'signup.bak.php');
		 require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'forgot_password.php');
		return ob_get_clean();
    }

    public function upgrade($url,$data_user, $transaction) {
		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'upgrade.php');
		// require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.php');
		return ob_get_clean();
    }

    public function success_upgrade($url,$data_user, $transaction) {
		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'success_upgrade.php');
		// require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.php');
		return ob_get_clean();
    }

    public function topup($url,$data_user, $transaction) {
		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'topup.php');
		// require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.php');
		return ob_get_clean();
    }

    public function success_topup($url,$data_user, $transaction) {
		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'success_topup.php');
		// require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.php');
		return ob_get_clean();
    }

     public function withdraw($url,$data_user, $withdraw) {
		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'withdraw.bak.php');
		// require_once($_SERVER['DOCUMENT_ROOT'].$this->dir_temp.'test.php');
		return ob_get_clean();
    }
}